/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  About.c - About-Requester anzeigen
**
**  COPYRIGHT (C) 1989-1993 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
*/

#include <proto/exec.h>
#include <proto/intuition.h>
#include <intuition/intuition.h>

#include "Bobi.h"

char nametext1[] = "Chris Haller, René Straub";
char nametext2[] = "René Straub, Chris Haller";

#include "AboutWindow.h"

static char *t1 = nametext1, *t2 = nametext2;

void AboutFunc(void)
{
	extern struct Screen *mainscreen;
	extern char idstring[];
	char *tmp;

	struct Window *w;

	LockWindows();
	NewWindowStructure1.Screen = mainscreen;
	IText2.IText = idstring;
	IText5.IText = t1;
	if (w = OpenWindow(&NewWindowStructure1))
	{
		PrintIText(w->RPort, &IntuiTextList1, 0L, 0L);
		WaitPort(w->UserPort);
		CloseWindow(w);
	}
	UnLockWindows();

	/* Namen tauschen */
	tmp = t1; t1 = t2; t2 = tmp;
}
