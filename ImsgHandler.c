#include <proto/exec.h>
#include <proto/intuition.h>

#include "Bobi.h"
#include "BobStructure.h"

extern struct MenuItem ToolWindowMenuItem,LayerModeMenuItem,
						OrgGridMenuItem,BobBordersMenuItem,
						CollisionRectMenuItem;
extern struct Screen *mainscreen;
extern struct MyBob *BobTable[];
extern WORD options,mainx0,mainy0,actbobnum,numbobs;
extern UWORD mainpalette[];
extern struct Gadget PosPropGadget,AutoSizeGadget,AutoOrgGadget,AnimKeyGadget;

/************************************************************************/

void HandleImsg(struct IntuiMessage *imsg,struct Menu *menustrip)
{
	register struct MyBob *bob;
	register ULONG class;
	register UWORD code;
	register WORD hitflags;

	class = imsg->Class;
	code  = imsg->Code;
	bob   = BobTable[actbobnum];

	switch(class)
	{
		case CLOSEWINDOW:
			ToolWindowMenuItem.Flags &= ~CHECKED;
			ToolWindowFunc();
			break;

		case MOUSEBUTTONS:
			switch(code)
			{
			case SELECTDOWN:
				if((options & GOF_LAYERMODE) && bob)
				{
					hitflags=BobHit(bob,imsg->MouseX,imsg->MouseY);
					if(!hitflags) DragBobFunc();
					else
					{
						if(hitflags&1) bob->Y0++;
						if(hitflags&2) bob->Y0--;
						if(hitflags&4) bob->X0++;
						if(hitflags&8) bob->X0--;
					}
				}
				break;

			case MENUUP:
				LoadPalette(mainpalette);
				break;

			default:
				break;
			}
			break;

		case GADGETDOWN:
			if(imsg->IAddress == (APTR)&PosPropGadget)
			{
				MovePosFunc();
			}
			break;

		case GADGETUP:
			if(bob)
			{
				if(AutoSizeGadget.Flags & SELECTED)
					bob->Flags |=  BOBF_AUTOSIZE;
				else
					bob->Flags &= ~BOBF_AUTOSIZE;

				if(AutoOrgGadget.Flags & SELECTED)
					bob->Flags |=  BOBF_AUTOORG;
				else
					bob->Flags &= ~BOBF_AUTOORG;

				if(AnimKeyGadget.Flags & SELECTED)
					bob->Flags |=  BODF_ANIMKEY;
				else
					bob->Flags &= ~BODF_ANIMKEY;
			}

			HandleEvent((APTR)imsg->IAddress);
			break;

		case MENUPICK:
			LoadPalette(mainpalette);
			if(code != MENUNULL)
				HandleEvent((APTR)ItemAddress(menustrip,(LONG)code));
			break;

		case RAWKEY:
#ifdef DEBUG
			if(code&0x80)
			{
				printf("Rohe Taste losgelassen: $%02lx\n",code&0x7f);
			}
			else
#endif
			{
				switch(code)
				{
					case 0x5f:			/* HELP */
						AboutFunc();
						break;

					case 0x50:			/* F1 GetBob */
						GetBobFunc();
						break;

					case 0x51:			/* F2 Get multiple bobs */
						GetMultiFunc();
						break;

					case 0x52:			/* F3 SetOrg */
						SetOrgFunc();
						break;

					case 0x53:			/* F4 Set CollisionBounds */
						SetCollBoundsFunc();
						break;

					case 0x58:			/* F9 Insert New */
						InsertNewBobFunc();
						break;

					case 0x59:			/* F10 DeleteBob */
						DeleteActBobFunc();
						break;

					case 0x4c:			/* Cursor rauf: 1. Bob */
						actbobnum=0;
						RefreshBobNum();
						break;

					case 0x4d:			/* Cursor runter: letztes Bob */
						actbobnum=numbobs;
						RefreshBobNum();
						break;

					case 0x4e:			/* Cursor rechts: 1 Bob vor */
						if(actbobnum<numbobs) actbobnum++;
						RefreshBobNum();
						break;

					case 0x4f:			/* Cursor links: 1 Bob zurück */
						if(actbobnum>0) actbobnum--;
						RefreshBobNum();
						break;

					case 0x3e:			/* NumPad 8: Bob rauf schieben */
						if(bob)
							/* if((mainy0-bob->Y0) > MINY) */
								bob->Y0++;
						break;

					case 0x1e:			/* NumPad 2: Bob runter */
						if(bob)
							/* if((mainy0+bob->Height-bob->Y0) < MAXY) */
								bob->Y0--;
						break;

					case 0x2d:			/* NumPad 4: Bob links */
						if(bob)
							/* if((mainx0-bob->X0) > MINX) */
								bob->X0++;
						break;

					case 0x2f:			/* NumPad 6: Bob rechts */
						if(bob)
							/* if((mainx0+bob->Width-bob->X0) < MAXX) */
								bob->X0--;
						break;

					case 0x2e:		/* NumPad 5: Bob zentrieren gemäss Modus */
						if(bob)
						{
							SetBobDefaultOrg(bob);
						}
						break;

					case 0x40:			/* Space-Taste : Redraw screen */
						Cleanup(FALSE);	
						OpenMain();
						LoadPalette(mainpalette);
						break;

					case 0x01:			/* Normale '1': SetFirstAnimBob */
						SetFirstBobFunc();
						break;

					case 0x02:			/* Normale '2': SetLastAnimBob */
						SetLastBobFunc();
						break;

					case 0x20:			/* 'A': Start Animation */
						StartAnimFunc();
						break;

					case 0x11:			/* 'W': Tool Window on/off */
						ToolWindowMenuItem.Flags ^= CHECKED;
						ToolWindowFunc();
						break;

					case 0x35:			/* 'B': Bob Border on/off */
						BobBordersMenuItem.Flags ^= CHECKED;
						break;

					case 0x33:			/* 'C': Collision Border on/off */
						CollisionRectMenuItem.Flags ^= CHECKED;
						break;

					case 0x28:			/* 'L': Layer mode on/off */
						LayerModeMenuItem.Flags ^= CHECKED;
						break;

					case 0x18:			/* 'O': Org Grid on/off */
						OrgGridMenuItem.Flags ^= CHECKED;
						break;

					case 0x19:			/* 'P': Edit Palette */
						EditPaletteFunc();
						break;

					case 0x13:			/* 'R': Rotate */
						RotateFunc();
						break;

					case 0x32:			/* 'X': Flip X */
						FlipXFunc();
						break;

					case 0x15:			/* 'Y' (on ASCII keyboards ONLY) */
						FlipYFunc();
						break;

					case 0x31:			/* 'Z' (on ASCII keyboards ONLY) */
						ZoomFunc();
						break;

					default:
						/* DisplayBeep(mainscreen); */
						/* printf("Rohe Taste gedrückt: $%02lx\n",code); */
						break;
				}
			}
			break;

		default:
		{
			char buf[100];
			sprintf(buf,"HandleImsg(): class=$%08lx, code=$%04lx",class,code);
			ShowMonoReq2(buf);
		}
		break;
	}
}
