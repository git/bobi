#include <exec/types.h>
#include <exec/memory.h>
#include <graphics/gfx.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/mathffp.h>
#include <proto/mathtrans.h>

#include "Bobi.h"
#include "BobStructure.h"
#include "ByteMap.h"
#include "ZoomWindow.h"

extern struct MyBob *BobTable[];
extern struct Screen *mainscreen;
extern struct RastPort *mainrastport;
extern UWORD mainpalette[];
extern WORD actbobnum,mainx0,mainy0;

/*************************************************************************/

static void ZoomBitMap(struct BitMap *bim, LONG faktor)
{
	register struct ByteMap *sbym,*dbym;	/* Source & Destination */

	if(sbym=MakeByteMap((WORD)(bim->BytesPerRow*8),bim->Rows))
	{
		if(dbym=MakeByteMap((WORD)(bim->BytesPerRow*8),bim->Rows))
		{
			BitMapToByteMap(bim,sbym);
			ZoomByteMap(sbym,dbym,faktor);
			ByteMapToBitMap(dbym,bim);
			FreeByteMap(dbym);
		}
		else ShowMonoReq2("No memory for zoom");
		FreeByteMap(sbym);
	}
	else ShowMonoReq2("No memory for zoom");
}

/*************************************************************************/

static struct MyBob *ZoomBob(struct MyBob *bob,register LONG faktor)
{
	register struct BitMap *sbim,*dbim;
	register WORD width,height;

	if((bob->Flags & BOBF_AUTOSIZE) && (faktor>100))
	{
		width  = (bob->Width*faktor)/100+1;
		height = (bob->Height*faktor)/100+1;
	}
	else
	{
		width = bob->Width; height = bob->Height;
	}

	if(sbim=BobToBitMap(bob))
	{
		if(dbim=MakeBitMap(width,height,bob->Depth))
		{
			BltBitMap(sbim,0,0,dbim,(width-bob->Width)/2,
				(height-bob->Height)/2,bob->Width,bob->Height,0xc0,0xff,0);
			WaitBlit();
			ZoomBitMap(dbim,faktor);
			bob=BitMapToBob(bob,dbim,width);
			MyFreeBitMap(dbim);
		}
		else bob = 0;
		MyFreeBitMap(sbim);
	}
	else bob = 0;
	return(bob);
}

/*************************************************************************/

void ZoomFunc()
{
	register struct Window *w;
	register WORD flag = 0;
	register LONG i,increment;

	if(BobTable[actbobnum])
	{
		LockWindows();
		NewWindowStructure1.Screen = mainscreen;
		if(w=OpenWindow(&NewWindowStructure1))
		{
			do
			{
				register struct IntuiMessage *msg;

				WaitPort(w->UserPort);
				msg=(struct IntuiMessage *)GetMsg(w->UserPort);
				if(msg->Class == CLOSEWINDOW)
				{
					flag = -1;
				}
				else if(msg->Class == GADGETUP)
				{
					if(msg->IAddress == (APTR)&OKGadget)
					{
							flag = 1;
					}
					else if(msg->IAddress == (APTR)&CancelGadget)
					{
							flag = -1;
					}
				}
			} while(!flag);
			CloseWindow(w);
			LoadPalette(mainpalette);

			if(flag>0)
			{
				register struct MyBob *bob;

				increment=LastGadgetSInfo.LongInt-FirstGadgetSInfo.LongInt;
				increment/=FramesGadgetSInfo.LongInt;

				for(i=0; i<FramesGadgetSInfo.LongInt; ++i)
				{
					if(bob=ZoomBob(BobTable[actbobnum],i*increment))
					{
						ShowBob(bob);
						InsertBob(bob,(WORD)(actbobnum+i+1));
					}
					else
					{
						ShowMonoReq2("No memory for new bobs, Zoom aborted.");
						break;
					}
				}

				for(i=1; i<=FramesGadgetSInfo.LongInt; ++i)
				{
					if(bob=AutoResizeBob(BobTable[actbobnum+i]))
					{
						FreeBob(BobTable[actbobnum+i]);
						BobTable[actbobnum+i] = bob;
						ShowBob(bob);
					}
					else ShowMonoReq2("Zoom:noresize");
				}
			}
		}
		UnLockWindows();
	}
	else ShowMonoReq2("Select a bob to zoom!");
}

