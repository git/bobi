/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  BobStructure.h - MyBob, BobFileHeader, BobData
**
**  COPYRIGHT (C) 1989-1993 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
*/

struct MyBob
{
	PLANEPTR	Planes[8];		/* Alle Planes sind aneinander!! */
	PLANEPTR	Mask;			/* Zeiger auf Masken-Plane */
	WORD		BytesPerRow;	/* Aufgerundetes ((Width+15)/8)&~1 */
	WORD		Width;
	WORD		Height;
	WORD		Depth;
	WORD		Flags;			/* Siehe BOBF_ Definitionen */
	WORD		PlaneSize;		/* BytesPerRow*Height */

	WORD		X0;				/* Nullpunkt X-Koordinate */
	WORD		Y0;				/* Nullpunkt Y-Koordinate */
	WORD		CollX0;			/* X0 des Kollisions-Bereiches */
	WORD		CollY0;			/* Y0 des Kollisions-Bereiches */
	WORD		CollX1;			/* X1 des Kollisions-Bereiches */
	WORD		CollY1;			/* Y1 des Kollisions-Bereiches */

	BYTE		PlanePick;		/* Für welche Planes existieren Daten */
	BYTE		PlaneOnOff;		/* wie bei Image Struktur */
	char		SourceLabel[64];
};

/* Folgende Flags (MyBob.Flags) sind nur für Bobi's Gebrauch */

#define BOBF_AUTOSIZE  1		/* Bob-Größe wird automatisch errechnet */
#define BOBF_AUTOORG   2		/* Bob bekommt neuen Org beim Manipulieren */
#define BOB_PRIVATEMASK 0xff	/* Diese Flags werden nicht generiert */

/* Folgende Flags (MyBob.Flags) sind für Bobol's Gebrauch und werden generiert */

#define BODF_ANIMKEY 256		/* 1. Bob einer Animation */


/*************************************************************************/

struct BobFileHeader
{
	LONG	Magic;				/* Muss BF_MAGIC sein */
	WORD	Version;			/* Versionsnummer des File-Formats */
	WORD	NumBobs;
	WORD	ColorTable[32];
	char	OutputName[127];
	BYTE	DefaultOrg;			/* 0=OL, 1=OM, 2=OR, 3=ML, 4=M, 5=MR, usw. */
	WORD	OutputFlags;		/* Siehe OF_ - Definitionen */
	char	SectionName[64];
	char	DefaultLabel[64];
	WORD	FirstAnimBob;
	WORD	LastAnimBob;
	UWORD	AnimSpeed;
	WORD	AnimFlags;			/* Siehe AF_ - Definitionen */
	WORD	GlobalOptions;		/* Siehe GO_ - in MyFunctions.h */
	WORD	MainX0,MainY0;		/* ORG-Werte, z.B. von SetMainOrgFunc() */
	BYTE	reserved[2];
};

#define BF_MAGIC 0x42424932		/* 'BBI2' */
#define BF_VERSION 111			/* Version des File-Formats */


/* Bit-Definitionen für das OutputFlags-Feld der BobFileHeader-Struktur: */

#define OF_ASSEMBLER 1
#define OF_OBJECT 2
#define OF_RAWDATA 4

#define OF_COLORTABLE 16
#define OF_BOBDATA 32
#define OF_BOBMASK 64

#define OF_GENERATEBOBS 256		/* Bobs generieren */
#define OF_GENERATESPRITES 512	/* Sprites generieren */


/* Bit-Definitionen für das AnimFlags-Feld der BobFileHeader-Struktur: */

#define AF_FORWARD 1			/* Animations-Richtung vorwärts */
#define AF_BACKWARD 2			/* Animations-Richtung rückwärts */
#define AF_BOUNCE 4				/* Modus: Normal/Bounce */


/*************************************************************************/

struct BobData	/* Steht im Source vor jedem Bob */
{
	WORD	Width;		/* Breite in Pixel */
	WORD	Height;		/* Höhe in Linien */
	WORD	X0;			/* X-Offset des Bob-Nullpunkts */
	WORD	Y0;			/* Y-Offset des Bob-Nullpunkts */
	WORD	CollX0;		/* X0 des Kollisions-Bereiches */
	WORD	CollY0;		/* Y0 des Kollisions-Bereiches */
	WORD	CollX1;		/* X1 des Kollisions-Bereiches */
	WORD	CollY1;		/* Y1 des Kollisions-Bereiches */
	BYTE	PlanePick;	/* Für welche Planes sind Daten vorhanden */
	BYTE	PlaneOnOff;	/* Was tun mit den restlichen Planes */
	WORD	Flags;		/* Verschiedene Flags, siehe BODF_ weiter oben */
	WORD	WordSize;	/* Bob-Breite in WORDs +1 */
	WORD	PlaneSize;	/* Anzahl Bytes bis zur nächsten Plane */
	WORD	TotalSize;	/* Komplette Größe des Bobs/Sprites mit Header */
};

