#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/dos.h>
#include <proto/intuition.h>
#include <intuition/intuitionbase.h>
#include <libraries/iff.h>
#include <graphics/gfxbase.h>

#include "Bobi.h"


extern struct Screen *mainscreen;
extern struct Screen *picturescreen;
extern struct Window *picturewindow;

extern UWORD mainpalette[],picturepalette[];

extern char PicPath[];	/* Definiert in LoadBobs.c wegen '.bobi' */
extern char PicName[];

extern BYTE arexxflag;
extern char arexxfilename[];


/****************************************************************************
**	Ein IFF-Bild von Disk laden und auf Screen anzeigen
*/

void LoadPicFunc()
{
	ULONG				*formptr;
	struct IFFL_BMHD	*bmhd;
	LONG				count;
	IFFL_HANDLE			ifffile;
	char				*name;

	LockWindows();

	if(arexxflag)
		name = arexxfilename;
	else
		name=FileRequest("Load Picture from Disk", "LOAD", PicPath, PicName);

	if(name)
	{
		if(ifffile = IFFL_OpenIFF(name, IFFL_MODE_READ))
		{
			formptr = ifffile;
			if(formptr[2]==ID_ANIM)	/* ANIMs enthalten ein FORM ILBM nach dem FORM ANIM */
				formptr+=3;

			if(bmhd = IFFL_GetBMHD(formptr))
			{
				static struct TagItem mytagitems[] =
				{
					SA_Overscan,OSCAN_TEXT,
					SA_AutoScroll,TRUE,
				};
				struct ExtNewScreen ns;
				struct NewWindow nw;

				ClearMem(&ns,sizeof(ns));
				ClearMem(&nw,sizeof(nw));

				if(OSVERSION(37))
				{
					ns.Width	= bmhd->w;
					ns.Height	= bmhd->h;
				}
				else
				{
					ns.Width    = GfxBase->NormalDisplayColumns>>1;
					ns.Height   = GfxBase->NormalDisplayRows;
				}
				ns.Depth        = bmhd->nPlanes;
				ns.ViewModes	= IFFL_GetViewModes(formptr);
				ns.Type         = CUSTOMSCREEN|SCREENQUIET|SCREENBEHIND|NS_EXTENDED;
				ns.Extension	= mytagitems;
				nw.IDCMPFlags   = IDCMP_MOUSEBUTTONS | IDCMP_MOUSEMOVE | IDCMP_DELTAMOVE;
				nw.Flags        = WFLG_BACKDROP | WFLG_BORDERLESS
									| WFLG_SIMPLE_REFRESH | WFLG_NOCAREREFRESH
									| WFLG_REPORTMOUSE | WFLG_RMBTRAP;
				nw.Type         = CUSTOMSCREEN;

				CloseScreenFunc();

				if(picturescreen = OpenScreen(&ns))
				{
					nw.Screen = picturescreen;
					nw.Width  = picturescreen->Width;
					nw.Height = picturescreen->Height;
					if(picturewindow = OpenWindow(&nw))
					{
						ShowTitle(picturescreen,FALSE);
						count = IFFL_GetColorTab(formptr,(WORD *)picturepalette);
						if(count>32)
							count = 32;
						CopyMem(picturepalette,mainpalette,count*2);
						LoadRGB4(&picturescreen->ViewPort,picturepalette,count);
						WaitBlit();	/* sonst ist der obere Teil des Bildes manchmal kaputt ?! */

						if(IFFL_DecodePic(formptr,&(picturescreen->BitMap)))
						{
							if(!arexxflag)
							{
								ScreenToFront(picturescreen);
								MoveScreen(mainscreen,0,-255);
								MoveScreen(mainscreen,0,255);
								Delay(15);
								ScreenToFront(mainscreen);
								for(count=28; count>0; --count)
									MoveScreen(mainscreen,0,-10);
							}
						}
						else
						{
							CloseScreenFunc();
							ShowIFFError("Error decoding picture");
						}
					}
					else
					{
						CloseScreenFunc();
						ShowMonoReq2("Can't open window!");
					}
				}
				else
				{
					CloseScreenFunc();
					ShowMonoReq2("Can't open screen!");
				}
			}
			else ShowIFFError("Mangled IFF picture");
			IFFL_CloseIFF(ifffile);
		}
		else ShowIFFError(name);
	}

	UnLockWindows();
}


/****************************************************************************
**	Picture-Screen schliessen und Bild freigeben
*/

void CloseScreenFunc()
{
	if(picturewindow)
	{
		CloseWindow(picturewindow);
		picturewindow = 0;
	}

	if(picturescreen)
	{
		CloseScreen(picturescreen);
		picturescreen = 0;
	}
}

