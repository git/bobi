/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  BMapSupport.c - Erstellen und Vernichten von BitMaps und ByteMaps
**
**  COPYRIGHT (C) 1989-1993 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
*/

#include <proto/exec.h>
#include <exec/memory.h>
#include <graphics/gfx.h>
#include <proto/graphics.h>
#include "ByteMap.h"


struct BitMap *MakeBitMap(WORD width, WORD height, WORD depth)
{
	register long i;
	register struct BitMap *b;
	register WORD planesize;

	if(b = AllocMem(sizeof(struct BitMap),MEMF_CLEAR))
	{
		InitBitMap(b,depth,width,height);
		planesize=b->BytesPerRow*b->Rows;

		if(b->Planes[0]=AllocMem(planesize*depth,MEMF_CHIP|MEMF_CLEAR))
		{
			for(i=1; i<depth; ++i)
				b->Planes[i] = b->Planes[0]+i*planesize;
		}
		else
		{
			FreeMem(b,sizeof(*b));
			b=0;
		}
	}
	return(b);
}


/*************************************************************************/

void MyFreeBitMap(struct BitMap *b)
{
	FreeMem(b->Planes[0],b->BytesPerRow*b->Rows*b->Depth);
	FreeMem(b,sizeof(*b));
}


/*************************************************************************/

struct ByteMap *MakeByteMap(register WORD width,register WORD height)
{
	register struct ByteMap *b;
	register long planesize;

	planesize = width*height;

	if(b = AllocMem(sizeof(struct ByteMap),MEMF_CLEAR))
	{
		if(b->Plane = AllocMem(planesize,MEMF_CLEAR))
		{
			b->Width = width;
			b->Height = height;
			b->PlaneSize = planesize;
		}
		else
		{
			FreeMem(b,sizeof(*b));
			b=0;
		}
	}
	return b;
}


/*************************************************************************/

void FreeByteMap(struct ByteMap *b)
{
	if(b->Plane)
	{
		FreeMem(b->Plane,b->PlaneSize);
	}
	FreeMem(b,sizeof(*b));
}
