/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  Get.c - Bob aus einem Bild ausschneiden
**
**  COPYRIGHT (C) 1989-1993 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
*/

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <graphics/rastport.h>

#include "Bobi.h"
#include "BobStructure.h"

extern struct MyBob		*BobTable[];
extern struct Screen	*mainscreen,*picturescreen;
extern struct Window	*mainwindow,*toolwindow,*picturewindow;
extern WORD				numbobs,actbobnum;
extern struct Gadget	AutoSizeGadget;
extern char				LabelGadgetSIBuff[];

static int x0,x1,y0,y1;

/****************************************************************************
**	Der Benutzer kann auf dem Picture-Screen ein Bob ausschneiden,
**	die Koordinaten werden in x0/y0/x1/y1 gespeichert
*/

static BOOL CutBob(BOOL multiflag)
{
	struct IntuiMessage *msg;
	BOOL looping=TRUE, okflag=FALSE;

	x0 = -1;					/* Startwerte, gegen MultiCutNoMove-Bug */
	x1 = picturewindow->MouseX;
	y1 = picturewindow->MouseY;
	DrawCross(picturescreen,x1,y1);

	ScreenToFront(picturescreen);
	ActivateWindow(picturewindow);
	ModifyIDCMP(picturewindow,MOUSEMOVE+MOUSEBUTTONS);
	do
	{
		WaitPort(picturewindow->UserPort);
		while(msg = (struct IntuiMessage *)GetMsg(picturewindow->UserPort))
		{
			if(msg->Class == MOUSEBUTTONS)
			{
				switch(msg->Code)
				{
					case SELECTDOWN:
						x0 = x1;
						y0 = y1;
						DrawCross(picturescreen,x0,y0);
						break;

					case SELECTUP:
						okflag=TRUE;		/* hier kein break! */

					default:
						looping=FALSE;
						break;
				}
				break;
			}
			else /* Muss MOUSEMOVE sein */
			{
				DrawCross(picturescreen,x1,y1);
				x1 = MIN(msg->MouseX, picturescreen->Width-1);
				y1 = MIN(msg->MouseY, picturescreen->Height-1);
				DrawCross(picturescreen,x1,y1);
			}
			ReplyMsg((struct Message *)msg);
		}
	} while(looping);

	DrawCross(picturescreen,x1,y1);
	DrawCross(picturescreen,x0,y0);
	ModifyIDCMP(picturewindow,0);

	if(!multiflag)	/* Falls nur 1 Bob zu cutten: mainscreen hervorbringen */
	{
		ScreenToFront(mainscreen);
		ActivateWindow(toolwindow?toolwindow:mainwindow);
	}

	if(x0>x1) {	WORD dummy=x0; x0=x1; x1=dummy; }
	if(y0>y1) {	WORD dummy=y0; y0=y1; y1=dummy; }

	return okflag;
}

/****************************************************************************
**	Bob an der gewünschten Stelle des Bildes ausschneiden
*/

BOOL GetBob(int x,int y,int w,int h)
{
	BOOL success = FALSE;

	if(picturescreen == NULL) return FALSE;		/* Nix auszuschneiden! */

	if(w<0)
	{
		x += w; w = -w;
	}

	if(h<0)
	{
		y += h; h = -h;
	}

	if(AutoSizeGadget.Flags & SELECTED)
	{
		int x1 = x+w-1, y1 = y+h-1;

		while((x<x1) && (w>0))
		{
			int i;

			for(i=y; i<y1; i++)
			{
				if(ReadPixel(&picturescreen->RastPort,x,i))
					goto xlgot;
			}
			x++; w--;
		}
		xlgot:

		while((y<y1) && (h>0))
		{
			int i;

			for(i=x; i<x1; i++)
			{
				if(ReadPixel(&picturescreen->RastPort,i,y))
					goto ylgot;
			}
			y++; h--;
		}
		ylgot:

		while(w>0)
		{
			int i;

			for(i=y; i<y1; i++)
			{
				if(ReadPixel(&picturescreen->RastPort,x1-1,i))
					goto xhgot;
			}
			x1--; w--;
		}
		xhgot:

		while(h>0)
		{
			int i;

			for(i=x; i<x1; i++)
			{
				if(ReadPixel(&picturescreen->RastPort,i,y1-1))
					goto yhgot;
			}
			y1--; h--;
		}
		yhgot: ;

	}	/* if(autosize) */

	if((w>0) && (h>0))
	{
		struct BitMap *tmpmap;

		if(tmpmap=MakeBitMap(w,h,picturescreen->BitMap.Depth))
		{
			struct MyBob *bob;

			BltBitMap(&(picturescreen->BitMap),x,y,tmpmap,0,0,w,h,0xc0,0xff,0);
			WaitBlit();
			if(bob=BitMapToBob(BobTable[actbobnum],tmpmap,w))
			{
				if(numbobs<MAXNUMBOBS)
				{
					if(BobTable[actbobnum])	/* Falls Bob existiert */
					{
						FreeBob(BobTable[actbobnum]);
					}
					else	/* Neues Bob an Tabellenende anhängen */
					{
						numbobs++;
						sprintf(bob->SourceLabel,LabelGadgetSIBuff,actbobnum);
						RefreshBobNum();	/* Prop-Gadget anpassen */
					}
					BobTable[actbobnum] = bob;
					ShowBob(bob);
					success = TRUE;
				}
				else ShowMonoReq2("Too many bobs!");
			}
			else ShowMonoReq2("Not enough memory for this bob");
			MyFreeBitMap(tmpmap);
		}
		else ShowMonoReq2("Not enough memory for tmp BitMap");
	}

	return success;
}

/****************************************************************************
**	Aus Bild ausschneiden und Bob generieren
*/

static BOOL GetOneBob(BOOL multiflag)
{
	if(picturescreen)
	{
		if(CutBob(multiflag))
		{
			return GetBob(x0,y0,x1-x0+1,y1-y0+1);
		}
		else			/* CutBob()==0: mit rechter Maustaste abgebrochen */
		{
			if(multiflag)
			{
				ScreenToFront(mainscreen);
				ActivateWindow(toolwindow?toolwindow:mainwindow);
			}
			else ShowMonoReq2("Operation cancelled");
		}
	}
	else ShowMonoReq2("No picture loaded, use 'Load IFF'");

	return FALSE;
}

/****************************************************************************
**	Ein Bob ausschneiden
*/

void GetBobFunc()
{
	GetOneBob(FALSE);
}

/****************************************************************************
**	Mehrere Bobs ausschneiden
*/

void GetMultiFunc()
{
	while(GetOneBob(TRUE)) actbobnum++;
	if(actbobnum>0) --actbobnum;
	RefreshBobNum();
}

