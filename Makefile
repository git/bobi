##
##  Bobi - The Ultimate Amiga Bob Manipulator
##
##  COPYRIGHT (C) 1992-1994 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
##  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
##  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
##  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
##


ALL:		Bobi


#############################################################################
##	Parameters und Flags

PROJECT		= Bobi

ASM		= Genam
AFLAGS		= -iINCLUDE: -l

CC		= sc
CFLAGS		=

LD		= SLINK
LDDEF		= _STACKSIZE=20000 __XCEXIT=@Exit
LFLAGS		= NOICONS ADDSYM MAP RAM:$(PROJECT).map


#############################################################################
##	Regeln

.c.o:
		$(CC) $(CFLAGS) $*.c

.S.o:
		-@Delete $@ 
		$(ASM) $(AFLAGS) $*.S


#############################################################################
## Module für Bobi:

LIBS		= LIB:scmffp.lib LIB:sc.lib LIB:small.lib LIB:debug.lib

MODS		= Bobi.o About.o Anim.o AREXX.o Bob.o BMapSupport.o ByteMap.o\
		  Color.o FileRequest.o Generate.o Get.o IFFAnim.o IFFError.o\
		  IMSGHandler.o Layer.o LoadBobs.o Misc.o Picture.o PropGadgets.o\
		  Rotate.o Sleep.o Stubs.o Zoom.o\
		  ConvertDate.o CreateRastPort.o GadgetSupport.o ShowRequest.o Snooze.o


#############################################################################
## Abhängigkeiten:

$(MODS):	Bobi.h BobStructure.h
Bobi.o:		MainWindow.h
About.o:	AboutWindow.h
Generate.o:	GenerateWindow.h
Rotate.o:	RotateWindow.h

Startup.o:	Startup.S
		-@Delete $@ 
		$(ASM) $(AFLAGS) -e DETACH=1 -e TINY=1 $*.S


#############################################################################
## Targets:

Bobi:		Startup.o $(MODS) $(LIBS)
		-@Delete $@ 
		$(LD) $(LFLAGS) FROM Startup.o $(MODS) TO $@ LIB $(LIBS) DEFINE $(LDDEF)


install:	Bobi
		$(LD) FROM Bobi TO SYS:C-User/Proprietary/Bobi NOICONS NODEBUG

clean:
		-@Delete *.o Bobi

dist:
		-@Delete Bobi.LHA
		-@LHA -x -a a Bobi.LHA c:Bobi Bobi.doc BobiTest.rexx BobiTest.bobs BobiTest.pic
		-@LHA v Bobi.LHA

