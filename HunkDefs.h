#define HUNK_UNIT 999
#define HUNK_NAME 1000
#define HUNK_CODE 1001
#define HUNK_DATA 1002
#define HUNK_BSS 1003
#define HUNK_RELOC32 1004
#define HUNK_RELOC16 1005
#define HUNK_RELOC8 1006
#define HUNK_EXT 1007
#define HUNK_SYMBOL 1008
#define HUNK_DEBUG 1009
#define HUNK_END 1010
#define HUNK_HEADER 1011
#define HUNK_OVERLAY 1013
#define HUNK_BREAK 1014

#define HUNKF_CHIP 0x40000000

#define EXT_SYMB 0
#define EXT_DEF 0x01000000
#define EXT_ABS 0x02000000
#define EXT_RES 0x03000000
#define EXT_REF32 0x81000000
#define EXT_COMMON 0x82000000
#define EXT_REF16 0x83000000
#define EXT_REF8 0x84000000

