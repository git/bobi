/* $VER: 3.2 - File 'Bobi_rev.h' */
#define	VERSION		3
#define	REVISION	2
#define	DATE	"22.6.20"
#define	VERS	"Bobi 3.2"
#define	VSTRING	"Bobi 3.2 (22.6.20)\n\r"
#define	VERSTAG	"\0$VER: Bobi 3.2 (22.6.20)"
/* Bumper 37.116 (12.7.91) was here */
