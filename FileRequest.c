/****** FileRequest *********************************************************
*
*   NAME
*       FileRequest -- Bring up an ASL file requester
*
*   SYNOPSIS
*       name = FileRequest(title, postext, path, name)
*
*       STRPTR FileRequest(STRPTR, STRPTR, STRPTR, STRPTR);
*
*   FUNCTION
*       Shows a file requester.
*
*   INPUTS
*       title    - The window title
*       postext  - Text of the left gadget
*       path     - Buffer for the path. This will be modifierd.
*       name     - Buffer for the name. This will be modifierd.
*
*   RESULTS
*       pathname - Pathname of the selected file, or NULL if cancel.
*
*   SEE ALSO
*       asl.library
*
****************************************************************************/

#include <exec/execbase.h>
#include <proto/dos.h>
#include <dos/dosextens.h>
#include <proto/asl.h>
#include <libraries/asl.h>

#include <string.h>


extern struct TextAttr	MyScreenTextAttr;
extern struct Screen	*mainscreen;


STRPTR FileRequest(STRPTR wtitle, STRPTR postext, STRPTR drawer, STRPTR file)
{
	if (AslBase)
	{
		struct FileRequester	*f;
		char					*filename=NULL;
		static char				buf[256];

		if (f = AllocAslRequestTags( ASL_FileRequest,
			ASLFR_Screen,			mainscreen,
			ASLFR_TextAttr,			&MyScreenTextAttr,
			ASLFR_TitleText,		wtitle,
			ASLFR_InitialFile,		file,

			(drawer != NULL) ? ASLFR_InitialDrawer : ASLFR_DrawersOnly,
			(drawer != NULL) ? drawer : (STRPTR)TRUE,
			TAG_DONE
		))
		{
			if (AslRequest(f, NULL))
			{
				strcpy(drawer, f->fr_Drawer);
				strcpy(file,   f->fr_File);
				strcpy(buf, drawer );
				AddPart(buf, file, sizeof(buf));
				filename = buf;
			}

			FreeAslRequest(f);
		}

		return filename;
	}
	else return "";
}


