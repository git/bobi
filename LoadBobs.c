#include <exec/types.h>
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <graphics/gfx.h>
#include <string.h>

#include "Bobi.h"
#include "BobStructure.h"

extern struct MyBob		*BobTable[];
extern struct Screen	*mainscreen;

extern WORD	options,numbobs,actbobnum,firstanimbob,lastanimbob,animflags;
extern BYTE defaultorg;
extern WORD	mainpalette[];
extern char	idstring[],outputname[],sectionname[];
extern WORD	outputflags,mainx0,mainy0;
extern char	LabelGadgetSIBuff[];

extern BYTE	arexxflag;
extern char arexxfilename[];

static char BobPath[128];		/* Load / Save Bobs */
static char BobName[80];
static char InsertPath[128];	/* Insert Bobs */
static char InsertName[80];
static char OPath[128];			/* Load / Save Offsets */
static char OName[80];

char PicPath[128];				/* Benötigt von Picture.c */
char PicName[80];

/****************************************************************************
**	Konfigurations-File laden und auswerten
*/

void LoadConfigFile(char *name)
{
	char buf[500];
	register BPTR file;

	ClearMem(buf,sizeof(buf));

	if(file=Open(name,MODE_OLDFILE))
	{
		register char *sptr,*dptr;

		Read(file,buf,490);
		Close(file);

		sptr=buf;
		dptr=BobPath; while((*dptr=*sptr++)>' ') dptr++; *dptr='\0';
		dptr=PicPath; while((*dptr=*sptr++)>' ') dptr++; *dptr='\0';
		strcpy(InsertPath,BobPath);
	}
}

/****************************************************************************
**	Ein Bob aus file laden
*/

static struct MyBob *LoadOneBob(register BPTR file,register WORD version)
{
	register struct MyBob *bob;
	register long i;

	if(bob=AllocMem(sizeof(struct MyBob),MEMF_CLEAR))
	{
		Read(file,bob,sizeof(*bob));
		if(version<108)
		{
			char buf[66];
			Seek(file,-8,OFFSET_CURRENT);	/* Antiskip Collsion Coords */
			CopyMem(&bob->CollX0,buf,66);
			CopyMem(buf,&bob->PlanePick,66);
			bob->CollX0=bob->CollX1=bob->CollY0=bob->CollY1=0;
		}

		if(bob->Planes[0] = AllocMem(bob->PlaneSize*bob->Depth,0L))
		{
			for(i=1; i<bob->Depth; ++i)
				bob->Planes[i] = bob->Planes[0]+i*bob->PlaneSize;
			Read(file,bob->Planes[0],bob->PlaneSize*bob->Depth);

			bob->Mask = 0;	/* jaja! */
			return bob;
		}
		else
		{
			FreeMem(bob,sizeof(*bob));
			ShowMonoReq2("Not enough memory for bob planes!");
		}
	}
	else
	{
		ShowMonoReq2("Not enough memory for bob structure!");
	}
	return 0;
}

/****************************************************************************
**	Bobs laden
*/

void LoadBobsFunc()
{
	LockWindows();
	LoadBobs(arexxflag ? arexxfilename
		: FileRequest("Load Bobs from Disk", "LOAD", BobPath, BobName));
	UnLockWindows();
}


void LoadBobs(char *name)
{
	struct BobFileHeader bfh;
	register struct MyBob *bob;
	register BPTR file;
	register long i;
	char buf[64];

	LockWindows();

	if(name)
	{
		if(file=Open(name,MODE_OLDFILE))
		{
			Read(file,&bfh,sizeof(bfh));
			if(bfh.Magic == BF_MAGIC)
			{
				if((bfh.Version>=106) && (bfh.Version<=BF_VERSION))
				{
					if(bfh.Version < 110)
					{
						ShowMonoReq2("Warning:\nThis is an old Bobi file.\n"
								"MainOrg is not saved.");
					}

					if(bfh.Version < 109)
					{
						ShowMonoReq2("Warning:\nThis is a very old Bobi file.\n"
								"AnimKey bobs are not marked.");
					}

					if(bfh.Version < 108)
					{
						ShowMonoReq2("Warning:\nThis is a very very old Bobi file.\n"
								"Collision info is not set.");
					}

					if(bfh.Version < 107)
					{
						ShowMonoReq2("Warning:\nThis is a very very very old Bobi file!\n"
								"Bob/Sprite flags and Collision info\n"
								"may be wrong.");
						bfh.OutputFlags |= OF_GENERATEBOBS;
					}
					ClearAll();

					if(bfh.Version >= 110)
					{
						mainx0 = bfh.MainX0;
						mainy0 = bfh.MainY0;
					}

					if(bfh.Version >= 111)
					{
						defaultorg = bfh.DefaultOrg;
					}

					CopyMem(bfh.ColorTable,mainpalette,2*32);
					strcpy(outputname,bfh.OutputName);
					strcpy(sectionname,bfh.SectionName);
					SetGlobalOptions(bfh.GlobalOptions,defaultorg);
					outputflags = bfh.OutputFlags;
					strcpy(LabelGadgetSIBuff,bfh.DefaultLabel);
					firstanimbob = bfh.FirstAnimBob;
					lastanimbob  = bfh.LastAnimBob;
					SetAnimSpeed(bfh.AnimSpeed);
					SetAnimFlags(bfh.AnimFlags);

					numbobs = bfh.NumBobs;
					for(i=0; i<bfh.NumBobs; ++i)
					{
						if(bob=LoadOneBob(file,bfh.Version)) BobTable[i]=bob;
						else
						{
							numbobs = i;
							sprintf(buf,"%ld of %ld bobs loaded.",i,bfh.NumBobs);
							ShowMonoReq2(buf);
							break;
						}
					}
					RefreshBobNum();
				}
				else if(bfh.Version > BF_VERSION)
				{
					ShowMonoReq2("You need a newer Bobi version\nto handle this file!");
				}
				else ShowMonoReq2("This is a mangled or\ntoo old Bobi file!");
			}
			else
			{
				sprintf(buf,"This is not a %s file !",idstring);
				ShowMonoReq2(buf);
			}
			Close(file);
		}
		else ShowFileError(name);
	}
	UnLockWindows();
}

/****************************************************************************
**	Bobs aus File an aktueller Position einfügen
*/

void InsertBobsFunc()
{
	struct BobFileHeader bfh;
	register char *name;
	register struct MyBob *bob;
	register BPTR file;
	register long i;
	char buf[64];

	LockWindows();

	if(arexxflag)
		name = arexxfilename;
	else
		name = FileRequest("Insert Bobs from Disk", "INSERT", InsertPath, InsertName);

	if(name)
	{
		if(file=Open(name,MODE_OLDFILE))
		{
			Read(file,&bfh,sizeof(bfh));
			if(bfh.Magic == BF_MAGIC)
			{
				if((bfh.Version>=106) && (bfh.Version<=BF_VERSION))
				{
					for(i=0; i<bfh.NumBobs; ++i)
					{
						if(bob=LoadOneBob(file,bfh.Version))
						{
							InsertBob(bob,(WORD)(actbobnum+i));
						}
						else
						{
							sprintf(buf,"%ld of %ld bobs inserted.",i,bfh.NumBobs);
							ShowMonoReq2(buf);
							break;
						}
					}
					RefreshBobNum();
				}
				else if(bfh.Version > BF_VERSION)
				{
					ShowMonoReq2("You need a newer Bobi version\nto handle this file!");
				}
				else ShowMonoReq2("This is a mangled or\ntoo old Bobi file!");
			}
			else
			{
				sprintf(buf,"This is not a %s file !",idstring);
				ShowMonoReq2(buf);
			}
			Close(file);
		}
		else ShowFileError(name);
	}
	UnLockWindows();
}

/****************************************************************************
**	Bobs in eine Datei speichern
*/

void SaveBobsFunc()
{
	struct BobFileHeader bfh;
	register char *name;
	register BPTR file;
	register long i;

	ClearMem(&bfh,sizeof(bfh));
	LockWindows();

	if(arexxflag)
		name = arexxfilename;
	else
		name = FileRequest("Save Bobs to Disk", "SAVE", BobPath, BobName);

	if(name)
	{
		bfh.Magic         = BF_MAGIC;
		bfh.Version       = BF_VERSION;
		bfh.NumBobs       = numbobs;
		bfh.OutputFlags   = outputflags;
		bfh.FirstAnimBob  = firstanimbob;
		bfh.LastAnimBob   = lastanimbob;
		bfh.AnimSpeed     = GetAnimSpeed();
		bfh.AnimFlags     = animflags;
		bfh.DefaultOrg    = defaultorg;
		bfh.GlobalOptions = options;
		bfh.MainX0        = mainx0;
		bfh.MainY0        = mainy0;

		CopyMem(mainpalette,bfh.ColorTable,2*32);
		strcpy(bfh.OutputName,outputname);
		strcpy(bfh.SectionName,sectionname);
		strcpy(bfh.DefaultLabel,LabelGadgetSIBuff);

		if(file=OpenNewFileSafely(name))
		{
			Write(file,&bfh,sizeof(bfh));
			for(i=0; i<numbobs; ++i)
			{
				Write(file,BobTable[i],sizeof(struct MyBob));
				Write(file,BobTable[i]->Planes[0],BobTable[i]->PlaneSize*BobTable[i]->Depth);
			}
			Close(file);
		}
	}
	UnLockWindows();
}

/****************************************************************************
**	Eine Datei von der Disk löschen
*/

void DeleteFileFunc()
{
	register char *name;
	char buf[160];

	LockWindows();
	if (name = FileRequest("Delete File on Disk", "DELETE", BobPath, BobName))
	{
		sprintf(buf,"Do you really want to delete the\nfile '%s' ?",name);
		if(ShowRequest2(buf,"YEP!"))
		{
			if(DeleteFile(name))
			{
//				sprintf(buf,"%s%s.fd",BobPath,*BobPath?"/":"");
//				DeleteFile(buf);
			}
			else ShowFileError(name);
		}
	}
	UnLockWindows();
}

/****************************************************************************
**	Bob-Offset-Tabelle von Disk laden
*/

void LoadOffsetsFunc()
{
	register char *name;
	register BPTR file;
	register long i;

	LockWindows();
	OPath[0] = OName[0] = '\0';

	if(arexxflag)
		name = arexxfilename;
	else
		name = FileRequest("Load Bob Offsets from Disk", "LOAD", OPath, OName);

	if(name)
	{
		if(file=Open(name,MODE_OLDFILE))
		{
			for(i=0; i<numbobs; ++i)
			{
				Read(file,&BobTable[i]->X0,2);
				Read(file,&BobTable[i]->Y0,2);
			}
			Close(file);
		}
		else ShowFileError(name);
	}
	UnLockWindows();
}

/****************************************************************************
**	Bob-Offsets in Datei speichern
*/

void SaveOffsetsFunc()
{
	register char *name;
	register BPTR file;
	register long i;

	LockWindows();
	OPath[0] = OName[0] = '\0';

	if(arexxflag)
		name = arexxfilename;
	else
		name = FileRequest("Save Bob Offsets to Disk", "SAVE", OPath, OName);

	if(name)
	{
		if(file=OpenNewFileSafely(name))
		{
			for(i=0; i<numbobs; ++i)
			{
				Write(file,&BobTable[i]->X0,2);
				Write(file,&BobTable[i]->Y0,2);
//				sprintf(buf,"\t\tDC.W\t%ld,%ld\t; %s\n",
//					BobTable[i]->X0,BobTable[i]->Y0,BobTable[i]->SourceLabel);
//				Write(file,buf,strlen(buf));
			}
			Close(file);
		}
	}
	UnLockWindows();
}

