#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/mathffp.h>
#include <proto/mathtrans.h>
#include <exec/memory.h>
#include <graphics/gfx.h>

#include "Bobi.h"
#include "BobStructure.h"
#include "ByteMap.h"
#include "RotateWindow.h"

#define PI 3.14159265358979323846

extern struct MyBob *BobTable[];
extern struct Screen *mainscreen;
extern struct RastPort *mainrastport;
extern UWORD mainpalette[];
extern WORD actbobnum,mainx0,mainy0;

/*************************************************************************/

static void RotateBobs(register LONG numframes, register LONG totangle)
{
	struct ByteMap *sbym,*dbym;
	register struct BitMap *sbim,*dbim;
	register WORD width,height;
	register struct MyBob *bob,*sbob=BobTable[actbobnum];


	if(sbob->Flags & BOBF_AUTOSIZE)
	{
		register float wq,hq;
		wq = SPFlt(sbob->Width);  wq = SPMul(wq,wq);
		hq = SPFlt(sbob->Height); hq = SPMul(hq,hq);
		width = height = SPFix(SPSqrt(SPAdd(wq,hq)))+1;
	}
	else
	{
		width = sbob->Width; height = sbob->Height;
	}

	if(sbym=MakeByteMap(width,height))
	{
		if(dbym=MakeByteMap(width,height))
		{
			if(sbim=BobToBitMap(sbob))
			{
				if(dbim=MakeBitMap(width,height,sbob->Depth))
				{
					register LONG i;

					BltBitMap(sbim,0,0,dbim,(width-sbob->Width)/2,
						(height-sbob->Height)/2,sbob->Width,sbob->Height,0xc0,0xff,0);
					WaitBlit();
					BitMapToByteMap(dbim,sbym);
					for(i=numframes; i>0; --i)
					{
						register float winkel;
						register LONG si,co;

						winkel = SPMul((float)(totangle*i)/numframes,PI/180.0);
						si = SPFix(SPMul(SPSin(winkel),1024.0)); /* 10 Dualstellen */
						co = SPFix(SPMul(SPCos(winkel),1024.0));

						RotateByteMap(sbym,dbym,width/2,height/2,si,co);
						ByteMapToBitMap(dbym,dbim);

						if(bob=BitMapToBob(sbob,dbim,width))
						{
							ShowBob(bob);
							InsertBob(bob,(WORD)(actbobnum+1));
						}
						else
						{
							ShowMonoReq2("No memory for new bobs,\nrotation cancelled.");
							break;
						}
					}
					MyFreeBitMap(dbim);
				}
				else ShowMonoReq2("Not enough memory for rotate!");
				MyFreeBitMap(sbim);
			}
			else ShowMonoReq2("Not enough memory for rotate!");
			FreeByteMap(dbym);
		}
		else ShowMonoReq2("Not enough memory for rotate!");
		FreeByteMap(sbym);
	}
	else ShowMonoReq2("Not enough memory for rotate!");
}

/*************************************************************************/

void RotateFunc()
{
	register struct Window *w;

	if(!BobTable[actbobnum])
	{
		ShowMonoReq2("Select a bob to rotate!");
		return;
	}

	LockWindows();
	NewWindowStructure1.Screen = mainscreen;
	if(w=OpenWindow(&NewWindowStructure1))
	{
		register WORD flag = 0;

		do
		{
			register struct IntuiMessage *msg;

			WaitPort(w->UserPort);
			msg=(struct IntuiMessage *)GetMsg(w->UserPort);
			if(msg->Class == CLOSEWINDOW)
			{
				flag = -1;
			}
			else if(msg->Class == GADGETUP)
			{
				if(msg->IAddress == (APTR)&OKGadget)
				{
						flag = 1;
				}
				else if(msg->IAddress == (APTR)&CancelGadget)
				{
						flag = -1;
				}
			}
		} while(!flag);
		CloseWindow(w);
		LoadPalette(mainpalette);

		if(flag>0)
		{
			register LONG i;
			register struct MyBob *bob;

			RotateBobs(FramesGadgetSInfo.LongInt,AngleGadgetSInfo.LongInt);

			for(i=1; i<=FramesGadgetSInfo.LongInt; ++i)
			{
				if(bob=AutoResizeBob(BobTable[actbobnum+i]))
				{
					FreeBob(BobTable[actbobnum+i]);
					BobTable[actbobnum+i] = bob;
					ShowBob(bob);
				}
				else ShowMonoReq2("Rotate:noresize");
			}
		}
	}
	UnLockWindows();
}

/*************************************************************************/

static void FlipXYBob(BOOL xflag)
{
	register struct MyBob *bob;
	register struct BitMap *bim;
	struct ByteMap *bym;
	register struct MyBob *flippedbob;

	if(bob=BobTable[actbobnum])
	{
		if(bim=BobToBitMap(bob))
		{
			if(bym=MakeByteMap(bob->Width,bim->Rows))
			{
				BitMapToByteMap(bim,bym);
				if(xflag)
				{
					FlipXByteMap(bym);
					bob->X0 = bob->Width-bob->X0;
	/*				if(bob->X0 < 0) bob->X0 = 0;
	*/			}
				else
				{
					FlipYByteMap(bym);
					bob->Y0 = bob->Height-bob->Y0;
	/*				if(bob->Y0 < 0) bob->Y0 = 0;
	*/			}
				ByteMapToBitMap(bym,bim);
				FreeByteMap(bym);
				if(flippedbob=BitMapToBob(bob,bim,bob->Width))
				{
					FreeBob(bob);
					BobTable[actbobnum]=flippedbob;
				}
				else ShowMonoReq2("No memory to flip bob!");
			}
			else ShowMonoReq2("No memory to flip bob!");
			MyFreeBitMap(bim);
		}
		else ShowMonoReq2("No memory to flip bob!");
	}
	else ShowMonoReq2("Select a bob to flip, you fool!");
}

/*************************************************************************/

void FlipXFunc(void) { FlipXYBob(TRUE);  }
void FlipYFunc(void) { FlipXYBob(FALSE); }

