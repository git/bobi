/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  Bobi.h - Haupt-Include-File
**
**  COPYRIGHT (C) 1989-1993 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
*/

#include <dos/dos.h>
#include <libraries/iff.h>
#include <stdio.h>
#include <string.h>

#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#define ClearMem(buf,len) memset(buf, 0, len)


#define MAXNUMBOBS 500	/* Maximale Anzahl Bobs */


/****************************************************************************
**	Definition des für Bobs benützbaren Bereichs des Bobi-Screens
*/

#define MINX 1
#define MINY 12
#define MAXX 292
#define MAXY 197

/****************************************************************************
**	Bit-Definitionen für die globalen Optionen:
*/

#define GOF_LAYERMODE 1			/* Layer-Modus ein/aus */
#define GOF_ORGGRID 2			/* MainOrg-Fadenkreuz sichtbar */
#define GOF_BOBBORDERS 4		/* Bob-Rähmchen sichtbar */
#define GOF_COLLISIONRECT 8		/* Kollisions-Grenzen sichtbar */


/****************************************************************************
**	Aliases die das Leben leichter machen:
*/

#define MainNW NewWindowStructure1
#define ToolNW NewWindowStructure2
#define SIGMASK(w) (1L<<(w)->UserPort->mp_SigBit)


/****************************************************************************
**	MenuItem-Aliases
*/

#define ForwardItem SubItem1				/* Anim Modes */
#define BackwardItem SubItem2
#define PingPongItem SubItem3
#define FirstDefaultOrgSubItem SubItem4		/* 9 Items insgesamt */
#define ToolWindowMenuItem MenuItem1
#define LayerModeMenuItem MenuItem2			/* Layer Mode on/off */
#define OrgGridMenuItem MenuItem3			/* Main Org Grid */
#define BobBordersMenuItem MenuItem4		/* Gadget Borders */
#define CollisionRectMenuItem MenuItem5		/* Show Collision Bounds */


/*
**	Macro zum Abfragen der aktuellen Betriebssystem-Version
*/

#define OSVERSION(ver)	(IntuitionBase->LibNode.lib_Version >= (ver))


/********************** Menu 1: Project **********************************/

void ClearAllFunc(void);				/* Bobi.c */
void LoadBobsFunc(void);				/* LoadBobs.c */
void InsertBobsFunc(void);				/* LoadBobs.c */
void SaveBobsFunc(void);				/* LoadBobs.c */
void DeleteFileFunc(void);				/* LoadBobs.c */
void GenerateCodeFunc(void);			/* Generate.c */
void AboutFunc(void);					/* About.c */
void SleepFunc(void);					/* Sleep.c */
void QuitFunc(void);					/* Stubs.c */


/********************** Menu 2: Picture **********************************/

void LoadPicFunc(void);					/* Picture.c */
void GrabDPaintFunc(void);				/* Stubs.c */
void GrabScreenFunc(void);				/* Stubs.c */
void CloseScreenFunc(void);				/* Picture.c */


/********************** Menu 3: Bob **************************************/

void GetBobFunc(void);					/* Menu 3 & Gadget */
void GetMultiFunc(void);				/* Menu 3 & Gadget */
void SetOrgFunc(void);					/* Menu 3 & Gadget */
void SetCollBoundsFunc(void);			/* Menu 3 & Gadget */
void FlipXFunc(void);					/* Menu 3 */
void FlipYFunc(void);					/* Menu 3 */
void RotateFunc(void);					/* Rotate.c */
void ZoomFunc(void);					/* Zoom.c */
void InsertNewBobFunc(void);			/* Menu 3 */
void DeleteActBobFunc(void);			/* Menu 3 */


/********************** Menu 4: Anim *************************************/

void LoadIFFAnimFunc(void);				/* Menu 4 */
void SaveIFFAnimFunc(void);				/* Menu 4 */
void SetFirstBobFunc(void);				/* Menu 4 */
void SetLastBobFunc(void);				/* Menu 4 */
void StartAnimFunc(void);				/* Menu 4 */
void SetAnimModeFunc(void);				/* Menu 4 */


/********************** Menu 5: Global ***********************************/

void ToolWindowFunc(void);				/* Stubs.c */
void SetMainOrgFunc(void);				/* Menu 5 */
void EditPaletteFunc(void);				/* Menu 5 */
void NewCLIFunc(void);					/* Menu 5 */
void LoadOffsetsFunc(void);				/* Menu 5 */
void SaveOffsetsFunc(void);				/* Menu 5 */
void RemakeLabelsFunc(void);			/* Menu 5 */
void RemakeCollisionFunc(void);			/* Menu 5 */


/********************** Globale Prozeduren *******************************/

/***** Anim.c: *****/

UWORD GetAnimSpeed(void);
void SetAnimFlags(WORD);
void SetAnimSpeed(UWORD);


/***** ARexx.c: *****/

ULONG InitARexx(void);
ULONG CloseARexx(void);
void RexxMsgHandler(void);


/***** Bob.c: *****/

struct MyBob *MakeBob(WORD,WORD,WORD);
void FreeBob(struct MyBob *);
struct MyBob *BitMapToBob(struct MyBob *,struct BitMap *,WORD);
struct BitMap *BobToBitMap(struct MyBob *);
void DeleteBob(WORD);
void InsertBob(struct MyBob *,WORD);
void ShowBob(struct MyBob *);
void ShowFrame(WORD);
WORD BobHit(struct MyBob *,WORD,WORD);
struct MyBob *AutoResizeBob(struct MyBob *);
void SetBobDefaultOrg(struct MyBob *);


/***** Bobi.c: *****/

void ClearAll(void);
void OpenMain(void);
void Cleanup(BOOL);


/***** BMapSupport.c *****/

struct BitMap *MakeBitMap(WORD,WORD,WORD);
void MyFreeBitMap(struct BitMap *);
struct ByteMap *MakeByteMap(WORD,WORD);
void FreeByteMap(struct ByteMap *);


/***** ByteMap.a: *****/

void BitMapToByteMap(struct BitMap *,struct ByteMap *);
void BobToByteMap(struct MyBob *,struct ByteMap *);
void ByteMapToBitMap(struct ByteMap *,struct BitMap *);
BOOL AutoResizeByteMap(struct ByteMap *);
void RotateByteMap(struct ByteMap *,struct ByteMap *,LONG,LONG,LONG,LONG);
void FlipXByteMap(struct ByteMap *);
void FlipYByteMap(struct ByteMap *);
void ZoomByteMap(struct ByteMap *,struct ByteMap *,LONG);


/***** Color.c: *****/

WORD *RequestColor(struct Screen *);


/***** ConvertDate.S *****/

char  __regargs *ConvertDate(struct DateStamp *, char *);


/***** CreateRastPort.S ****/

struct RastPort * __regargs CreateRastPort(WORD, WORD, WORD);
void __regargs DeleteRastPort(struct RastPort *);


/***** FileRequest.c: *****/

STRPTR FileRequest(STRPTR wtitle, STRPTR postext, STRPTR drawer, STRPTR file);


/***** GadgetSupport.S *****/

BOOL __regargs DeselectGadget(struct Window *,struct Gadget *);
BOOL __regargs SelectGadget(struct Window *,struct Gadget *);


/***** Get.c: *****/

BOOL GetBob(int x,int y,int w,int h);


/***** LoadBobs.c: *****/

void LoadBobs(char *);
void LoadConfigFile(char *);


/***** Misc.c: *****/

void ShowMonoReq2(char *,...);
BOOL ShowRequest2(char *,char *);
void ShowFileError(char *);
BPTR OpenNewFileSafely(char *);
void DrawRect(struct RastPort *,WORD,WORD,WORD,WORD,WORD);
void DrawCross(struct Screen *,WORD,WORD);
void LoadPalette(UWORD *);
void LockWindows(void);
void UnLockWindows(void);
void SetGlobalOptions(WORD opts, BYTE defaultorg);
void GetGlobalOptions(void);


/***** PropGadgets.c: *****/

void MovePosFunc(void);
void RefreshBobNum(void);
WORD ReadBobNum(void);


/***** Snooze.S *****/

void __regargs Snooze(struct Window *);
void __regargs UnSnooze(struct Window *);


/***** Verschiedenes: *****/

void DragBobFunc(void);				/* Layer.c */
void HandleEvent(APTR);				/* MainWindow.h, PW2 */
void ShowIFFError(char *);			/* IFFError.c */
LONG ShowRequest(char *text, char *postext, char *negtext, ULONG flags);	/* ShowRequest.c */

