/*
**	In der 'ByteMap'-Struktur hat jeder Pixel ein Byte mit dem Farbwert.
**	Änderungen müssen auch in ByteMap.S übernommen werden.
*/

struct ByteMap
{
	WORD Width;			/* Breite der ByteMap in Pixel */
	WORD Height;		/* Höhe der ByteMap in Pixel */
	LONG PlaneSize;		/* Grösse für Alloc/FreeMem der Plane */
	APTR Plane;			/* Zeiger auf die aktuelle Datenplane */
};

