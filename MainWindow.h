
struct TextAttr TOPAZ80 = {
	(STRPTR)"topaz.font",
	TOPAZ_EIGHTY,0,0
};
struct NewScreen NewScreenStructure = {
	0,0,
	320,256,
	6,
	0,1,
	EXTRA_HALFBRITE,
	CUSTOMSCREEN,
	&TOPAZ80,
	NULL,
	NULL,
	NULL
};

#define NEWSCREENSTRUCTURE NewScreenStructure

USHORT Palette[] = {
	0x0000,
	0x0ECA,
	0x035D,
	0x0AAA,
	0x0D80,
	0x0FE0,
	0x08F0,
	0x0444,
	0x00B6,
	0x00DD,
	0x00AF,
	0x007C,
	0x000F,
	0x070F,
	0x0C0E,
	0x0C08,
	0x0620,
	0x0E52,
	0x0A52,
	0x0FCA,
	0x0333,
	0x0444,
	0x0555,
	0x0666,
	0x0777,
	0x0888,
	0x0999,
	0x0AAA,
	0x0CCC,
	0x014A,
	0x0B98,
	0x0C00
#define PaletteColorCount 32
};

#define PALETTE Palette

UBYTE UNDOBUFFER[64];

SHORT BorderVectors1[] = {
	0,0,
	25,0,
	25,10,
	0,10,
	0,0
};
struct Border Border1 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors1,
	NULL
};

struct Gadget Gadget3 = {
	NULL,
	-24,12,
	24,9,
	GADGHBOX+GADGHIMAGE+GRELRIGHT,
	NULL,
	BOOLGADGET,
	(APTR)&Border1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

SHORT BorderVectors2[] = {
	0,0,
	25,0,
	25,10,
	0,10,
	0,0
};
struct Border Border2 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors2,
	NULL
};

struct Gadget Gadget2 = {
	&Gadget3,
	-24,-9,
	24,9,
	GADGHBOX+GADGHIMAGE+GRELBOTTOM+GRELRIGHT,
	NULL,
	BOOLGADGET,
	(APTR)&Border2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

struct PropInfo PosPropGadgetSInfo = {
	AUTOKNOB+FREEVERT+PROPBORDERLESS,
	(UWORD)-1,(UWORD)-1,
	(UWORD)-1,(UWORD)-1,
};

struct Image Image1 = {
	0,0,
	22,161,
	0,
	NULL,
	0x0000,0x0000,
	NULL
};

struct Gadget PosPropGadget = {
	&Gadget2,
	-23,25,
	22,-39,
	GRELRIGHT+GRELHEIGHT,
	RELVERIFY+GADGIMMEDIATE,
	PROPGADGET,
	(APTR)&Image1,
	NULL,
	NULL,
	NULL,
	(APTR)&PosPropGadgetSInfo,
	NULL,
	NULL
};

#define GadgetList1 PosPropGadget

struct IntuiText IText1 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Remake Collision",
	NULL
};

struct MenuItem MenuItem15 = {
	NULL,
	-50,112,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText1,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText2 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Remake Labels",
	NULL
};

struct MenuItem MenuItem14 = {
	&MenuItem15,
	-50,104,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText2,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText3 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"-----------------",
	NULL
};

struct MenuItem MenuItem13 = {
	&MenuItem14,
	-50,96,
	136,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText3,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText4 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Save Offsets",
	NULL
};

struct MenuItem MenuItem12 = {
	&MenuItem13,
	-50,88,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText4,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText5 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Load Offsets",
	NULL
};

struct MenuItem MenuItem11 = {
	&MenuItem12,
	-50,80,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText5,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText6 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"-----------------",
	NULL
};

struct MenuItem MenuItem10 = {
	&MenuItem11,
	-50,72,
	136,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText6,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText7 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Edit Palette.. P",
	NULL
};

struct MenuItem MenuItem9 = {
	&MenuItem10,
	-50,64,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText7,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText8 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"-----------------",
	NULL
};

struct MenuItem MenuItem8 = {
	&MenuItem9,
	-50,56,
	136,8,
	ITEMTEXT+HIGHCOMP,
	0,
	(APTR)&IText8,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText9 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Set Main Org",
	NULL
};

struct MenuItem MenuItem7 = {
	&MenuItem8,
	-50,48,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText9,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText10 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"-----------------",
	NULL
};

struct MenuItem MenuItem6 = {
	&MenuItem7,
	-50,40,
	136,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText10,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText11 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Collision    C",
	NULL
};

struct MenuItem MenuItem5 = {
	&MenuItem6,
	-50,32,
	136,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText11,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText12 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Borders      B",
	NULL
};

struct MenuItem MenuItem4 = {
	&MenuItem5,
	-50,24,
	136,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText12,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText13 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Org Grid     O",
	NULL
};

struct MenuItem MenuItem3 = {
	&MenuItem4,
	-50,16,
	136,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText13,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText14 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Layer Mode   L",
	NULL
};

struct MenuItem MenuItem2 = {
	&MenuItem3,
	-50,8,
	136,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText14,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText15 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Tool Window  W",
	NULL
};

struct MenuItem MenuItem1 = {
	&MenuItem2,
	-50,0,
	136,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP+CHECKED,
	0,
	(APTR)&IText15,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct Menu Menu5 = {
	NULL,
	224,0,
	55,0,
	MENUENABLED,
	"Global",
	&MenuItem1
};

struct IntuiText IText16 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Start         A",
	NULL
};

struct MenuItem MenuItem22 = {
	NULL,
	-64,48,
	120,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText16,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText17 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Ping-Pong",
	NULL
};

struct MenuItem SubItem3 = {
	NULL,
	105,15,
	91,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText17,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText18 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Backward",
	NULL
};

struct MenuItem SubItem2 = {
	&SubItem3,
	105,7,
	91,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText18,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText19 = {
	2,1,JAM1,
	19,0,
	&TOPAZ80,
	"Forward",
	NULL
};

struct MenuItem SubItem1 = {
	&SubItem2,
	105,-1,
	91,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP+CHECKED,
	0,
	(APTR)&IText19,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText20 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Mode          »",
	NULL
};

struct MenuItem MenuItem21 = {
	&MenuItem22,
	-64,40,
	120,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText20,
	NULL,
	NULL,
	&SubItem1,
	MENUNULL
};

struct IntuiText IText21 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Last Bob      2",
	NULL
};

struct MenuItem MenuItem20 = {
	&MenuItem21,
	-64,32,
	120,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText21,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText22 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"First Bob     1",
	NULL
};

struct MenuItem MenuItem19 = {
	&MenuItem20,
	-64,24,
	120,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText22,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText23 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"---------------",
	NULL
};

struct MenuItem MenuItem18 = {
	&MenuItem19,
	-64,16,
	120,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText23,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText24 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Save IFF Anim..",
	NULL
};

struct MenuItem MenuItem17 = {
	&MenuItem18,
	-64,8,
	120,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText24,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText25 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Load IFF Anim..",
	NULL
};

struct MenuItem MenuItem16 = {
	&MenuItem17,
	-64,0,
	120,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText25,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct Menu Menu4 = {
	&Menu5,
	178,0,
	39,0,
	MENUENABLED,
	"Anim",
	&MenuItem16
};

struct IntuiText IText26 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Delete     F10",
	NULL
};

struct MenuItem MenuItem36 = {
	NULL,
	0,104,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText26,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText27 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Insert New  F9",
	NULL
};

struct MenuItem MenuItem35 = {
	&MenuItem36,
	0,96,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText27,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText28 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"--------------",
	NULL
};

struct MenuItem MenuItem34 = {
	&MenuItem35,
	0,88,
	112,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText28,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText29 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Zoom         Z",
	NULL
};

struct MenuItem MenuItem33 = {
	&MenuItem34,
	0,80,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText29,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText30 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Rotate       R",
	NULL
};

struct MenuItem MenuItem32 = {
	&MenuItem33,
	0,72,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText30,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText31 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Flip Y       Y",
	NULL
};

struct MenuItem MenuItem31 = {
	&MenuItem32,
	0,64,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText31,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText32 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Flip X       X",
	NULL
};

struct MenuItem MenuItem30 = {
	&MenuItem31,
	0,56,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText32,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText33 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"--------------",
	NULL
};

struct MenuItem MenuItem29 = {
	&MenuItem30,
	0,48,
	112,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText33,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText34 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Set Collis. F4",
	NULL
};

struct MenuItem MenuItem28 = {
	&MenuItem29,
	0,40,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText34,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText35 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem12 = {
	NULL,
	111,15,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText35,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText36 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem11 = {
	&SubItem12,
	104,15,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP+CHECKED,
	0,
	(APTR)&IText36,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText37 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem10 = {
	&SubItem11,
	97,15,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText37,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText38 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem9 = {
	&SubItem10,
	111,7,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText38,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText39 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem8 = {
	&SubItem9,
	104,7,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText39,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText40 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem7 = {
	&SubItem8,
	97,7,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText40,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText41 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem6 = {
	&SubItem7,
	111,-1,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText41,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText42 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem5 = {
	&SubItem6,
	104,-1,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText42,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText43 = {
	3,1,COMPLEMENT,
	0,0,
	NULL,
	".",
	NULL
};

struct MenuItem SubItem4 = {
	&SubItem5,
	97,-1,
	7,8,
	CHECKIT+ITEMTEXT+MENUTOGGLE+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText43,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText44 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"DefaultOrg   »",
	NULL
};

struct MenuItem MenuItem27 = {
	&MenuItem28,
	0,32,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText44,
	NULL,
	NULL,
	&SubItem4,
	MENUNULL
};

struct IntuiText IText45 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Set Org     F3",
	NULL
};

struct MenuItem MenuItem26 = {
	&MenuItem27,
	0,24,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText45,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText46 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"--------------",
	NULL
};

struct MenuItem MenuItem25 = {
	&MenuItem26,
	0,16,
	112,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText46,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText47 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Get multi   F2",
	NULL
};

struct MenuItem MenuItem24 = {
	&MenuItem25,
	0,8,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText47,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText48 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Get single  F1",
	NULL
};

struct MenuItem MenuItem23 = {
	&MenuItem24,
	0,0,
	112,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText48,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct Menu Menu3 = {
	&Menu4,
	140,0,
	31,0,
	MENUENABLED,
	"Bob",
	&MenuItem23
};

struct IntuiText IText49 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Close Screen",
	NULL
};

struct MenuItem MenuItem41 = {
	NULL,
	0,32,
	144,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText49,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText50 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"------------------",
	NULL
};

struct MenuItem MenuItem40 = {
	&MenuItem41,
	0,24,
	144,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText50,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText51 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Grab Screen..",
	NULL
};

struct MenuItem MenuItem39 = {
	&MenuItem40,
	0,16,
	144,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText51,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText52 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Grab DPaint",
	NULL
};

struct MenuItem MenuItem38 = {
	&MenuItem39,
	0,8,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText52,
	NULL,
	'D',
	NULL,
	MENUNULL
};

struct IntuiText IText53 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Load IFF..",
	NULL
};

struct MenuItem MenuItem37 = {
	&MenuItem38,
	0,0,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText53,
	NULL,
	'P',
	NULL,
	MENUNULL
};

struct Menu Menu2 = {
	&Menu3,
	70,0,
	63,0,
	MENUENABLED,
	"Picture",
	&MenuItem37
};

struct IntuiText IText54 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Quit",
	NULL
};

struct MenuItem MenuItem56 = {
	NULL,
	0,112,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText54,
	NULL,
	'Q',
	NULL,
	MENUNULL
};

struct IntuiText IText55 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"------------------",
	NULL
};

struct MenuItem MenuItem55 = {
	&MenuItem56,
	0,104,
	144,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText55,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText56 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Sleep",
	NULL
};

struct MenuItem MenuItem54 = {
	&MenuItem55,
	0,96,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText56,
	NULL,
	'`',
	NULL,
	MENUNULL
};

struct IntuiText IText57 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"------------------",
	NULL
};

struct MenuItem MenuItem53 = {
	&MenuItem54,
	0,88,
	144,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText57,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText58 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"About..",
	NULL
};

struct MenuItem MenuItem52 = {
	&MenuItem53,
	0,80,
	144,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText58,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText59 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"------------------",
	NULL
};

struct MenuItem MenuItem51 = {
	&MenuItem52,
	0,72,
	144,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText59,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText60 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Do Script..",
	NULL
};

struct MenuItem SubItem16 = {
	NULL,
	129,23,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText60,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText61 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"-----------------",
	NULL
};

struct MenuItem SubItem15 = {
	&SubItem16,
	129,15,
	136,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText61,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText62 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Stop Recording",
	NULL
};

struct MenuItem SubItem14 = {
	&SubItem15,
	129,7,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText62,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText63 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Start Recording..",
	NULL
};

struct MenuItem SubItem13 = {
	&SubItem14,
	129,-1,
	136,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText63,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText64 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"ARexx            »",
	NULL
};

struct MenuItem MenuItem50 = {
	&MenuItem51,
	0,64,
	144,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText64,
	NULL,
	NULL,
	&SubItem13,
	MENUNULL
};

struct IntuiText IText65 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Generate Code",
	NULL
};

struct MenuItem MenuItem49 = {
	&MenuItem50,
	0,56,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText65,
	NULL,
	'G',
	NULL,
	MENUNULL
};

struct IntuiText IText66 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"------------------",
	NULL
};

struct MenuItem MenuItem48 = {
	&MenuItem49,
	0,48,
	144,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText66,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText67 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Delete File..",
	NULL
};

struct MenuItem MenuItem47 = {
	&MenuItem48,
	0,40,
	144,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText67,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText68 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Save Bobs..",
	NULL
};

struct MenuItem MenuItem46 = {
	&MenuItem47,
	0,32,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText68,
	NULL,
	'S',
	NULL,
	MENUNULL
};

struct IntuiText IText69 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Insert Bobs..",
	NULL
};

struct MenuItem MenuItem45 = {
	&MenuItem46,
	0,24,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText69,
	NULL,
	'I',
	NULL,
	MENUNULL
};

struct IntuiText IText70 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Load Bobs",
	NULL
};

struct MenuItem MenuItem44 = {
	&MenuItem45,
	0,16,
	144,8,
	ITEMTEXT+COMMSEQ+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText70,
	NULL,
	'O',
	NULL,
	MENUNULL
};

struct IntuiText IText71 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"------------------",
	NULL
};

struct MenuItem MenuItem43 = {
	&MenuItem44,
	0,8,
	144,8,
	ITEMTEXT+HIGHCOMP+HIGHBOX,
	0,
	(APTR)&IText71,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct IntuiText IText72 = {
	2,1,JAM1,
	0,0,
	&TOPAZ80,
	"Clear All..",
	NULL
};

struct MenuItem MenuItem42 = {
	&MenuItem43,
	0,0,
	144,8,
	ITEMTEXT+ITEMENABLED+HIGHCOMP,
	0,
	(APTR)&IText72,
	NULL,
	NULL,
	NULL,
	MENUNULL
};

struct Menu Menu1 = {
	&Menu2,
	0,0,
	63,0,
	MENUENABLED,
	"Project",
	&MenuItem42
};

#define MenuList1 Menu1

struct NewWindow NewWindowStructure1 = {
	0,0,
	320,200,
	0,1,
	MOUSEBUTTONS+GADGETDOWN+GADGETUP+MENUPICK+RAWKEY,
	BACKDROP+REPORTMOUSE+BORDERLESS+ACTIVATE+NOCAREREFRESH,
	&PosPropGadget,
	NULL,
	NULL,
	NULL,
	NULL,
	0,0,
	0,0,
	CUSTOMSCREEN
};

SHORT BorderVectors6[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
struct Border Border6 = {
	134,11,
	1,0,JAM1,
	5,
	BorderVectors6,
	NULL
};

SHORT BorderVectors5[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
struct Border Border5 = {
	94,11,
	1,0,JAM1,
	5,
	BorderVectors5,
	&Border6
};

SHORT BorderVectors4[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
struct Border Border4 = {
	174,11,
	1,0,JAM1,
	5,
	BorderVectors4,
	&Border5
};

SHORT BorderVectors3[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
struct Border Border3 = {
	214,11,
	1,0,JAM1,
	5,
	BorderVectors3,
	&Border4
};

struct Gadget Gadget11 = {
	NULL,
	0,0,
	1,1,
	GADGHBOX+GADGHIMAGE,
	NULL,
	BOOLGADGET,
	(APTR)&Border3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

UBYTE LabelGadgetSIBuff[64];
struct StringInfo LabelGadgetSInfo = {
	LabelGadgetSIBuff,
	UNDOBUFFER,
	0,
	64,
	0,
	0,0,0,0,0,
	0,
	0,
	NULL
};

SHORT BorderVectors7[] = {
	0,0,
	157,0,
	157,11,
	0,11,
	0,1
};
struct Border Border7 = {
	-2,-2,
	1,0,JAM1,
	5,
	BorderVectors7,
	NULL
};

struct IntuiText IText73 = {
	1,0,JAM1,
	28,-11,
	NULL,
	"Source Label:",
	NULL
};

struct Gadget LabelGadget = {
	&Gadget11,
	96,46,
	154,9,
	NULL,
	RELVERIFY,
	STRGADGET,
	(APTR)&Border7,
	NULL,
	&IText73,
	NULL,
	(APTR)&LabelGadgetSInfo,
	NULL,
	NULL
};

SHORT BorderVectors8[] = {
	0,0,
	33,0,
	33,21,
	0,21,
	0,0
};
struct Border Border8 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors8,
	NULL
};

struct IntuiText IText75 = {
	1,0,JAM1,
	4,11,
	NULL,
	"Org",
	NULL
};

struct IntuiText IText74 = {
	1,0,JAM1,
	4,2,
	NULL,
	"Set",
	&IText75
};

struct Gadget SetOrgGadget = {
	&LabelGadget,
	255,35,
	32,20,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border8,
	NULL,
	&IText74,
	NULL,
	NULL,
	NULL,
	NULL
};

SHORT BorderVectors9[] = {
	0,0,
	33,0,
	33,21,
	0,21,
	0,0
};
struct Border Border9 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors9,
	NULL
};

struct IntuiText IText77 = {
	1,0,JAM1,
	4,11,
	NULL,
	"Bob",
	NULL
};

struct IntuiText IText76 = {
	1,0,JAM1,
	4,2,
	NULL,
	"Get",
	&IText77
};

struct Gadget GetBobGadget = {
	&SetOrgGadget,
	255,12,
	32,20,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border9,
	NULL,
	&IText76,
	NULL,
	NULL,
	NULL,
	NULL
};

struct PropInfo AnimSpeedGadgetSInfo = {
	AUTOKNOB+FREEVERT,
	(UWORD)-1,20755,
	(UWORD)-1,4095,
};

struct Image Image2 = {
	0,11,
	6,4,
	0,
	NULL,
	0x0000,0x0000,
	NULL
};

struct Gadget AnimSpeedGadget = {
	&GetBobGadget,
	4,11,
	14,45,
	NULL,
	NULL,
	PROPGADGET,
	(APTR)&Image2,
	NULL,
	NULL,
	NULL,
	(APTR)&AnimSpeedGadgetSInfo,
	NULL,
	NULL
};

SHORT BorderVectors10[] = {
	0,0,
	71,0,
	71,11,
	0,11,
	0,0
};
struct Border Border10 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors10,
	NULL
};

struct IntuiText IText78 = {
	1,0,JAM1,
	4,1,
	NULL,
	"Anim Key",
	NULL
};

struct Gadget AnimKeyGadget = {
	&AnimSpeedGadget,
	21,45,
	70,10,
	SELECTED,
	RELVERIFY+TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border10,
	NULL,
	&IText78,
	NULL,
	NULL,
	NULL,
	NULL
};

SHORT BorderVectors11[] = {
	0,0,
	71,0,
	71,11,
	0,11,
	0,0
};
struct Border Border11 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors11,
	NULL
};

struct IntuiText IText79 = {
	1,0,JAM1,
	4,1,
	NULL,
	"Auto Org",
	NULL
};

struct Gadget AutoOrgGadget = {
	&AnimKeyGadget,
	21,28,
	70,10,
	SELECTED,
	RELVERIFY+TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border11,
	NULL,
	&IText79,
	NULL,
	NULL,
	NULL,
	NULL
};

SHORT BorderVectors12[] = {
	0,0,
	71,0,
	71,11,
	0,11,
	0,0
};
struct Border Border12 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors12,
	NULL
};

struct IntuiText IText80 = {
	1,0,JAM1,
	4,1,
	NULL,
	"AutoSize",
	NULL
};

struct Gadget AutoSizeGadget = {
	&AutoOrgGadget,
	21,12,
	70,10,
	SELECTED,
	RELVERIFY+TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border12,
	NULL,
	&IText80,
	NULL,
	NULL,
	NULL,
	NULL
};

#define GadgetList2 AutoSizeGadget

struct NewWindow NewWindowStructure2 = {
	0,11,
	292,58,
	0,1,
	MOUSEBUTTONS+GADGETDOWN+GADGETUP+MENUPICK+CLOSEWINDOW+RAWKEY,
	WINDOWDRAG+WINDOWCLOSE+REPORTMOUSE+ACTIVATE+NOCAREREFRESH,
	&AutoSizeGadget,
	NULL,
	"Bobi Tool Window",
	NULL,
	NULL,
	0,0,
	0,0,
	CUSTOMSCREEN
};


void HandleEvent(object)
APTR object;
{
  if (object == (APTR)&GetBobGadget) { GetBobFunc(      ); return; }
  if (object == (APTR)&SetOrgGadget) { SetOrgFunc(      ); return; }
  if (object == (APTR)&MenuItem42) { ClearAllFunc(      ); return; }
  if (object == (APTR)&MenuItem44) { LoadBobsFunc(      ); return; }
  if (object == (APTR)&MenuItem45) { InsertBobsFunc(      ); return; }
  if (object == (APTR)&MenuItem46) { SaveBobsFunc(      ); return; }
  if (object == (APTR)&MenuItem47) { DeleteFileFunc(      ); return; }
  if (object == (APTR)&MenuItem49) { GenerateCodeFunc(      ); return; }
  if (object == (APTR)&MenuItem52) { AboutFunc(      ); return; }
  if (object == (APTR)&MenuItem54) { SleepFunc(      ); return; }
  if (object == (APTR)&MenuItem56) { QuitFunc(      ); return; }
  if (object == (APTR)&MenuItem37) { LoadPicFunc(      ); return; }
  if (object == (APTR)&MenuItem38) { GrabDPaintFunc(      ); return; }
  if (object == (APTR)&MenuItem39) { GrabScreenFunc(      ); return; }
  if (object == (APTR)&MenuItem41) { CloseScreenFunc(      ); return; }
  if (object == (APTR)&MenuItem23) { GetBobFunc(      ); return; }
  if (object == (APTR)&MenuItem24) { GetMultiFunc(      ); return; }
  if (object == (APTR)&MenuItem26) { SetOrgFunc(      ); return; }
  if (object == (APTR)&MenuItem28) { SetCollBoundsFunc(      ); return; }
  if (object == (APTR)&MenuItem30) { FlipXFunc(      ); return; }
  if (object == (APTR)&MenuItem31) { FlipYFunc(      ); return; }
  if (object == (APTR)&MenuItem32) { RotateFunc(      ); return; }
  if (object == (APTR)&MenuItem33) { ZoomFunc(      ); return; }
  if (object == (APTR)&MenuItem35) { InsertNewBobFunc(      ); return; }
  if (object == (APTR)&MenuItem36) { DeleteActBobFunc(      ); return; }
  if (object == (APTR)&MenuItem16) { LoadIFFAnimFunc(      ); return; }
  if (object == (APTR)&MenuItem17) { SaveIFFAnimFunc(      ); return; }
  if (object == (APTR)&MenuItem19) { SetFirstBobFunc(      ); return; }
  if (object == (APTR)&MenuItem20) { SetLastBobFunc(      ); return; }
  if (object == (APTR)&SubItem1) { SetAnimModeFunc(      ); return; }
  if (object == (APTR)&SubItem2) { SetAnimModeFunc(      ); return; }
  if (object == (APTR)&SubItem3) { SetAnimModeFunc(      ); return; }
  if (object == (APTR)&MenuItem22) { StartAnimFunc(      ); return; }
  if (object == (APTR)&MenuItem1) { ToolWindowFunc(      ); return; }
  if (object == (APTR)&MenuItem7) { SetMainOrgFunc(      ); return; }
  if (object == (APTR)&MenuItem9) { EditPaletteFunc(      ); return; }
  if (object == (APTR)&MenuItem11) { LoadOffsetsFunc(      ); return; }
  if (object == (APTR)&MenuItem12) { SaveOffsetsFunc(      ); return; }
  if (object == (APTR)&MenuItem14) { RemakeLabelsFunc(      ); return; }
  if (object == (APTR)&MenuItem15) { RemakeCollisionFunc(      ); return; }
}
#define HANDLEEVENT HandleEvent

/* end of PowerWindows source generation */
