#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include "Bobi.h"
#include "BobStructure.h"

extern struct MyBob *BobTable[];
extern struct Window *mainwindow;
extern struct Gadget PosPropGadget;
extern struct PropInfo PosPropGadgetSInfo;
extern WORD numbobs,actbobnum;

/*************************************************************************/

void MovePosFunc()
{
	register struct Message *msg;
	register WORD oldnum=-1;

	for(;;)
	{
		WaitTOF();
		if(msg = GetMsg(mainwindow->UserPort))
		{
			ReplyMsg(msg);
			break;
		}
		actbobnum = ReadBobNum();
		if(actbobnum != oldnum)
		{
			ShowFrame(actbobnum);
			/* ShowBob(BobTable[actbobnum]); */
			oldnum = actbobnum;
		}
	}
	actbobnum = ReadBobNum();
	ShowFrame(actbobnum);
}

/*************************************************************************/

WORD ReadBobNum()
{
	return((WORD)((((UWORD)numbobs*PosPropGadgetSInfo.VertPot)+0x7fffL)>>16));
}

/*************************************************************************/

void RefreshBobNum()
{
	PosPropGadgetSInfo.VertBody = 0xffffL/(numbobs+1);
	if(numbobs>0)
	{
		PosPropGadgetSInfo.VertPot  = (actbobnum*0xffffL)/numbobs;
	}
	else PosPropGadgetSInfo.VertPot = 0;

	RefreshGList(&PosPropGadget,mainwindow,0,1);
}
