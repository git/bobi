/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  Bobi.c - Hautprogramm
**
**  COPYRIGHT (C) 1989-2020 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
**
**  Modification History
**  --------------------
**
**  05-Mar-89  CHW   V2.01   Project started
**  11-Jun-89  CHW   V2.15   InsertBobs added
**  15-Jun-89  CHW   V2.16   SaveOffsets() added
**  25-Jun-89  CHW   V2.20   An ch.lib angepasst
**  04-Jul-89  CHW   V2.22   Generate Code Window versch?nert
**  07-Jul-89  CHW   V2.24   AutoResizeBob() implementiert
**  26-Jul-89  CHW   V2.30   Sprites, neues BobData-Format etc.
**  05-Nov-89  CHW   V2.36   bod_Flags implementiert, Lattice 5.04
**  07-Nov-89  CHW   V2.37   CollisionRects gr?sser als Bob m?glich
**  12-Nov-89  CHW   V2.38   Generiert EQU-Headerfile f?r RawData
**  17-Nov-89  CHW   V2.39   Default-Pfade werden aus '.bobi' gelesen
**  23-Nov-89  CHW   V2.40   Aufger?umt, Bobsize nicht mehr begrenzt,
**                           SetMainOrg() implementiert, 2 bugs fixed
**  30-Nov-89  CHW   V2.42   About() verbessert, Collision auto on
**  03-Dec-89  CHW   V2.43   Zoom funktioniert jetzt mit Clipping
**  14-Dec-89  CHW   V2.44   LayerMode- & CustomBitmap-Bug fixed
**  20-Jan-90  CHW   V2.99   ToolWindow implemented, LevelEd support added
**  04-Mar-90  CHW   V2.99c  DecodeAnim benutzt iff.library
**  09-Mar-90  CHW   V2.99d  Save Sprite OK f?r width < 16
**  03-Apr-90  CHW   V2.99e  Pfad bleibt erhalten (f?r CHH)
**  17-Jul-90  CHW   V2.99f  Default-Font definiert, Screen-Width var.
**  12-Feb-91  CHW   V2.99i  Bei Sprites wird auch eine BobData-
**                           Struktur definiert
**  18-Feb-92  CHW   V2.99j  Ich kann's wieder compilieren :-)
**  12-Apr-92  CHW   V2.99m  Picture-Screen scrollt unter 2.0
**  12-Apr-92  CHW   V2.99n  BitMap<->ByteMap k?nnen auch <5 Planes,
**                           deshalb haben Bobs jetzt nicht immer >=5 Pl.
**  20-May-92  CHW   V2.99o  GenerateCode-ARexx-Befehl eingebaut
**  06-Jun-92  CHW   V2.99p  Default-Org funktioniert jetzt
**  11-Jul-92  CHW   V2.99q  Main-Screen ist jetzt Halfbrite
**  02-Nov-93  CHW   V3.0    Compiliert mit sc6, ASL-Filerequester
**  13-Jun-20  CHW   V3.1    Kann beim Startup Bob-File laden (CLI+WB)
**  22-Jun-20  CHW   V3.2    IFF-Screen darf kleiner sein als Bobi-Screen, WaitBlit()
*/

#include <proto/exec.h>
#include <exec/libraries.h>
#include <proto/graphics.h>
#include <graphics/gfxbase.h>
#include <proto/intuition.h>
#include <intuition/intuitionbase.h>
#include <proto/dos.h>
#include <workbench/startup.h>

#include <string.h>
#include <libraries/iff.h>

#include "Bobi_rev.h"
#include "Bobi.h"
#include "BobStructure.h"
#include "MainWindow.h"

extern void exit(LONG);
extern void HandleImsg(struct IntuiMessage *, struct Menu *);

#define ns NewScreenStructure

extern struct Process *ProcessBase;		/* Zeiger auf unseren Prozess, siehe Startup.o */

struct Library *MathBase, *MathTransBase;


/****************************************************************************
**	Globale Variable
*/

struct Library	*AslBase, *IFFBase;

struct Screen	*mainscreen;
struct Window	*mainwindow, *toolwindow;
struct RastPort	*mainrastport, *toolrastport;

struct Screen	*picturescreen;
struct Window	*picturewindow;

struct MyBob	*BobTable[MAXNUMBOBS+3];

char idstring_versiontag[7] = { '\0', '$', 'V', 'E', 'R', ':', ' ' };
char idstring[] = VERS " (" DATE ")";		/* Wird ?berall verwendet */


WORD options;				/* Globale Optionen, siehe GO_ Bobi.h */
BYTE defaultorg;			/* 0=OL, 1=OM, 2=OR, 3=ML, 4=M, 5=MR, usw. */
WORD numbobs;				/* Momentane Anzahl benutzte Bobs */
WORD actbobnum;				/* Nummer des aktuellen Bobs */

UWORD mainpalette[64];		/* Palette des Bobi-Screens */
UWORD picturepalette[64];	/* Palette des geladenen Bildes */

char outputname[80];		/* Name des zu generierenden Files */
char sectionname[80];		/* Name der Section des zu generierenden Files */
WORD outputflags;			/* Sprache und so */

WORD mainwidth,mainheight;	/* Ausmasse des Screens */
WORD mainx0,mainy0;			/* Globaler Nullpunkt der Bobs */

WORD firstanimbob,lastanimbob,animspeed,animflags;

struct TextAttr MyScreenTextAttr =
{
	(STRPTR)"topaz.font", TOPAZ_EIGHTY, 0, 0
};

ULONG rexxsigmask;			/* ARexx Signal Bit Maske */


/****************************************************************************
**	L?scht alle Bobs
*/

void ClearAll()
{
	register long i;

	for(i=0; i<numbobs; ++i)
	{
		FreeBob(BobTable[i]);
		BobTable[i]=0;
	}
	numbobs   = 0;
	actbobnum = 0;
	SetGlobalOptions(GOF_LAYERMODE|GOF_BOBBORDERS|GOF_COLLISIONRECT,7);
	outputflags = 0;
	if(mainwindow) RefreshBobNum();

	firstanimbob=lastanimbob=animspeed=0;
	SetAnimFlags(AF_FORWARD);

	strcpy(outputname,"RAM:TestBobs.S");
	strcpy(sectionname,"BobData");
	strcpy(LabelGadgetSIBuff,"Bob%ld");
	if(!picturescreen)
	{
		CopyMem(Palette,mainpalette,PaletteColorCount*2);
		if(mainscreen) LoadPalette(mainpalette);
	}
}	


/****************************************************************************
**	Alles freigeben und evtl. Programm verlassen
*/

void Cleanup(BOOL reallyquit)
{
	if(reallyquit)
	{
		CloseARexx();		/* Rexx-Stuff freigeben */
		ClearAll();			/* Alle Bobs freigeben */
	}

	CloseScreenFunc();					/* Geladenes Bild freigeben */

	if(toolwindow)
	{
		ToolNW.TopEdge=toolwindow->TopEdge;
		ClearMenuStrip(toolwindow);
		CloseWindow(toolwindow);
		toolwindow=0; toolrastport=0;
	}

	if(mainwindow)
	{
		ClearMenuStrip(mainwindow);
		ProcessBase->pr_WindowPtr=0;
		CloseWindow(mainwindow);
		mainwindow=0; mainrastport=0;
	}

	if(mainscreen)
	{
		CloseScreen(mainscreen);
		mainscreen = NULL;
	}

	if(reallyquit)
	{
		if(IFFBase)
		{
			CloseLibrary(IFFBase);
			IFFBase = NULL;
		}

		if (AslBase)
		{
			CloseLibrary(AslBase);
			AslBase = NULL;
		}

		if(MathTransBase)
		{
			CloseLibrary(MathTransBase);
			MathTransBase = NULL;
		}

		if(MathBase)
		{
			CloseLibrary(MathBase);
			MathBase = NULL;
		}

		if(IntuitionBase)
		{
			CloseLibrary((struct Library *)IntuitionBase);
			IntuitionBase = NULL;
		}

		if(GfxBase)
		{
			CloseLibrary((struct Library *)GfxBase);
			GfxBase = NULL;
		}
		exit(0);
	}
}


/****************************************************************************
**	Fehlertext ausgeben und Programm verlassen
*/

void Fail(char *reason)
{
	char buf[200];
	sprintf(buf,"%s:\n%s\nPlease consult your manual.",idstring,reason);
	ShowRequest(buf,0,"OKAY",0x8000);
	Cleanup(TRUE);
}


/****************************************************************************
**	Gerufen bei Control-C-Signal
*/

void __stdargs __saveds _abort(void)
{
	if(CloseARexx())	/* D?rfen wir Quit machen ? */
	{
		Fail("*** BREAK  -  Bobi aborted.");
	}
}


/****************************************************************************
**	Screen und Window ?ffnen
*/

void OpenMain()
{
	ns.Width  = GfxBase->NormalDisplayColumns>>1;	/* /2 weil Lo-Res */
	ns.Height = STDSCREENHEIGHT;
	ns.Font   = &MyScreenTextAttr;
	ns.DefaultTitle = idstring;

	if(!(mainscreen = OpenScreen(&ns)))
	{
		Fail("Can't open screen");
	}
	LoadPalette(Palette);

	MainNW.Screen = mainscreen;
	MainNW.Width  = mainwidth  = mainscreen->Width;
	MainNW.Height = mainheight = mainscreen->Height;
	if(!(mainwindow = OpenWindow(&MainNW)))
	{
		Fail("Can't open window");
	}
	ProcessBase->pr_WindowPtr = (APTR)mainwindow;
	mainrastport = mainwindow->RPort;
	DrawRect(mainrastport,mainwidth-26,23,mainwidth-1,mainheight-13,1);

	ToolWindowFunc();		/* ToolWindow ?ffnen falls enabled */
}


/****************************************************************************
**	Hauptprogramm
*/

LONG Main(LONG arglen, char *argline)
{
	struct WBStartup *startup = (struct WBStartup *)argline;
	char filename[200] = "\0";	/* File beim Start laden? */

	LONG mainidcmpflags = MainNW.IDCMPFlags;
	LONG toolidcmpflags = ToolNW.IDCMPFlags;

	ToolNW.TopEdge=10000;	/* ToolWindow ganz unten */

	if(!(GfxBase = (struct GfxBase *)OpenLibrary("graphics.library",0L)))
		return 0;

	if(!(IntuitionBase = (struct IntuitionBase *)OpenLibrary("intuition.library",0L)))
		return 0;

	if(!(AslBase = OpenLibrary("asl.library", 0L)))
		Fail("Can't open asl.library");

	if(!(IFFBase = OpenLibrary(IFFNAME,0L)))
		Fail("Can't open iff.library");

	if(!(MathBase = OpenLibrary("mathffp.library",0L)))
		Fail("Can't open mathffp.library");

	if(!(MathTransBase = OpenLibrary("mathtrans.library",0L)))
		Fail("Can't open mathtrans.library");

	if(IFFBase->lib_Version < IFFVERSION)
		Fail("Your iff.library is too old");

	rexxsigmask=InitARexx();

	LoadConfigFile(".bobi");
	ClearAll();
	mainx0=MAXX/2; mainy0=(MAXY*2)/3;

	OpenMain();


	/*
	**	Wenn ein Argument angegben wurde, Bobs laden
	*/
	if (arglen > 0)		/* Start vom CLI */
	{
		strncpy(filename, argline, arglen);
		filename[arglen-1] = '\0';
	}
	else if (startup->sm_NumArgs > 1)	/* Started with some arguments */
	{
		struct WBArg *arg = startup->sm_ArgList+1;

		if (arg->wa_Lock)
			CurrentDir(arg->wa_Lock);

		if (arg->wa_Name && *arg->wa_Name)
			strcpy(filename, arg->wa_Name);
	}

	if (*filename)
		LoadBobs(filename);


	/*
	**	Tada! Die Hauptschleife
	*/
	for(;;)
	{
		struct IntuiMessage msgcopy,*msg;

		SetMenuStrip(mainwindow,&MenuList1);
		if(toolwindow) SetMenuStrip(toolwindow,&MenuList1);
		actbobnum=ReadBobNum();
		ShowFrame(actbobnum);
		ModifyIDCMP(mainwindow,mainidcmpflags|MENUVERIFY);
		if(toolwindow) ModifyIDCMP(toolwindow,toolidcmpflags|MENUVERIFY);

		for(;;)
		{
			if(toolwindow && (mainwindow->Flags & WINDOWACTIVE))
				ActivateWindow(toolwindow);
			if(msg=(struct IntuiMessage *)GetMsg(mainwindow->UserPort)) break;
			if(toolwindow)
				if(msg=(struct IntuiMessage *)GetMsg(toolwindow->UserPort))
					break;

			if(toolwindow) Wait(SIGMASK(mainwindow)|SIGMASK(toolwindow)|rexxsigmask);
			else Wait(SIGMASK(mainwindow)|rexxsigmask);
			RexxMsgHandler();
		}

		CopyMem(msg,&msgcopy,sizeof(msgcopy));
		ReplyMsg((struct Message *)msg);
		if(msgcopy.Class == MENUVERIFY)
		{
			LoadPalette(Palette);					/* Default-Farben */
		}
		else
		{
			ModifyIDCMP(mainwindow,mainidcmpflags);		/* MENUVERIFY off */
			if(toolwindow) ModifyIDCMP(toolwindow,toolidcmpflags);
			GetGlobalOptions();						/* options updaten */
			HandleImsg(&msgcopy,&MenuList1);
		}
	}

	return 0;
}
