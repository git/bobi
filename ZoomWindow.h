
static UBYTE LastGadgetSIBuff[5] =
	"200";
static struct StringInfo LastGadgetSInfo = {
	LastGadgetSIBuff,
	NULL,
	0,
	5,
	0,
	0,0,0,0,0,
	0,
	200,
	NULL
};

static SHORT BorderVectors1[] = {
	0,0,
	174,0,
	174,12,
	0,12,
	0,1
};
static struct Border Border1 = {
	-1,-3,
	3,0,JAM1,
	5,
	BorderVectors1,
	NULL
};

static struct IntuiText IText1 = {
	3,0,JAM1,
	4,0,
	NULL,
	"Last  (0-1000%)",
	NULL
};

static struct Gadget LastGadget = {
	NULL,
	6,33,
	173,11,
	NULL,
	RELVERIFY+STRINGRIGHT+LONGINT,
	STRGADGET,
	(APTR)&Border1,
	NULL,
	&IText1,
	NULL,
	(APTR)&LastGadgetSInfo,
	NULL,
	NULL
};

static UBYTE FramesGadgetSIBuff[5] =
	"20";
static struct StringInfo FramesGadgetSInfo = {
	FramesGadgetSIBuff,
	NULL,
	0,
	5,
	0,
	0,0,0,0,0,
	0,
	20,
	NULL
};

static SHORT BorderVectors2[] = {
	0,0,
	174,0,
	174,12,
	0,12,
	0,1
};
static struct Border Border2 = {
	-1,-3,
	3,0,JAM1,
	5,
	BorderVectors2,
	NULL
};

static struct IntuiText IText2 = {
	3,0,JAM1,
	4,0,
	NULL,
	"# of Frames",
	NULL
};

static struct Gadget FramesGadget = {
	&LastGadget,
	6,49,
	173,11,
	NULL,
	RELVERIFY+STRINGRIGHT+LONGINT,
	STRGADGET,
	(APTR)&Border2,
	NULL,
	&IText2,
	NULL,
	(APTR)&FramesGadgetSInfo,
	NULL,
	NULL
};

static UBYTE FirstGadgetSIBuff[5] =
	"0";
static struct StringInfo FirstGadgetSInfo = {
	FirstGadgetSIBuff,
	NULL,
	0,
	5,
	0,
	0,0,0,0,0,
	0,
	0,
	NULL
};

static SHORT BorderVectors3[] = {
	0,0,
	174,0,
	174,12,
	0,12,
	0,1
};
static struct Border Border3 = {
	-1,-3,
	3,0,JAM1,
	5,
	BorderVectors3,
	NULL
};

static struct IntuiText IText3 = {
	3,0,JAM1,
	4,0,
	NULL,
	"First (0-1000%)",
	NULL
};

static struct Gadget FirstGadget = {
	&FramesGadget,
	6,17,
	173,11,
	NULL,
	RELVERIFY+STRINGRIGHT+LONGINT,
	STRGADGET,
	(APTR)&Border3,
	NULL,
	&IText3,
	NULL,
	(APTR)&FirstGadgetSInfo,
	NULL,
	NULL
};

static SHORT BorderVectors4[] = {
	0,0,
	81,0,
	81,12,
	0,12,
	0,0
};
static struct Border Border4 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors4,
	NULL
};

static struct IntuiText IText4 = {
	1,0,JAM1,
	24,2,
	NULL,
	"ZOOM",
	NULL
};

static struct Gadget OKGadget = {
	&FirstGadget,
	6,-15,
	80,11,
	GRELBOTTOM,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border4,
	NULL,
	&IText4,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors5[] = {
	0,0,
	81,0,
	81,12,
	0,12,
	0,0
};
static struct Border Border5 = {
	-1,-1,
	31,0,JAM1,
	5,
	BorderVectors5,
	NULL
};

static struct IntuiText IText5 = {
	31,0,JAM1,
	16,2,
	NULL,
	"CANCEL",
	NULL
};

static struct Gadget CancelGadget = {
	&OKGadget,
	-85,-15,
	80,11,
	GRELBOTTOM+GRELRIGHT,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border5,
	NULL,
	&IText5,
	NULL,
	NULL,
	NULL,
	NULL
};

#define GadgetList1 CancelGadget

static struct NewWindow NewWindowStructure1 = {
	63,39,
	185,82,
	0,1,
	GADGETUP+CLOSEWINDOW,
	WINDOWDRAG+WINDOWDEPTH+WINDOWCLOSE+ACTIVATE+NOCAREREFRESH,
	&CancelGadget,
	NULL,
	"    Zoom Bobs    ",
	NULL,
	NULL,
	5,5,
	(UWORD)-1,(UWORD)-1,
	CUSTOMSCREEN
};
