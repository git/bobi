/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  IFFAnim.c - Bobs aus einer IFF-Animation ausschneiden
**
**  COPYRIGHT (C) 1989-1993 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
*/

#include <libraries/iff.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/dos.h>
#include <proto/intuition.h>

#include "Bobi.h"
#include "ByteMap.h"

extern struct Screen *mainscreen;
extern WORD actbobnum;

static char AnimPath[128];
static char AnimName[80];

/****************************************************************************
**	Ein Bob aus einer Animation ausschneiden
*/

static BOOL GetNextBob(struct BitMap *bim)
{
	struct ByteMap	*bym;
	struct MyBob	*bob;
	BOOL			success = FALSE;
	WORD			width;

	if(bym=MakeByteMap((WORD)(bim->BytesPerRow*8),bim->Rows))
	{
		BitMapToByteMap(bim,bym);
		if(!AutoResizeByteMap(bym))
		{
			bym->Width=1; bym->Height=1;
		}

		if(bim=MakeBitMap(bym->Width,bym->Height,(WORD)bim->Depth))
		{
			ByteMapToBitMap(bym,bim);
			width = bym->Width;
			FreeByteMap(bym);
			if(bob=BitMapToBob(0,bim,width))
			{
				InsertBob(bob,(WORD)(actbobnum++));
				success = TRUE;
			}
			else ShowMonoReq2("Not enough memory to insert bob");
			MyFreeBitMap(bim);
		}
		else
		{
			ShowMonoReq2("Not enough memory to insert bob");
			FreeByteMap(bym);
		}
	}
	else ShowMonoReq2("Not enough memory to insert bob");

	return success;
}

/****************************************************************************
**	IFF-Animation laden und ins Bobi-Format umwandeln
*/

void LoadIFFAnimFunc()
{
	ULONG				*ifffile,*formptr;
	struct IFFL_BMHD	*bmhd;
	LONG				count;
	char				*path;
	struct NewScreen	ns;
	struct Screen		*s1, *s2=NULL;
	UWORD				palette[128];

	LockWindows();
	if (path = FileRequest("Load IFF animation from Disk", "LOAD", AnimPath, AnimName))
	{
		if (ifffile = (ULONG *)IFFL_OpenIFF(path, IFFL_MODE_READ))
		{
			if(ifffile[2] == ID_ANIM)
			{
				formptr = ifffile+3;

				if(bmhd = IFFL_GetBMHD(formptr))
				{
					ClearMem(&ns,sizeof(ns));
					ns.Width  = (bmhd->w > 64) ? bmhd->w : 64;
					ns.Height = (bmhd->h > 64) ? bmhd->h : 64;
					ns.Depth  = (bmhd->nPlanes > 5) ? bmhd->nPlanes : 5;
					ns.ViewModes  = IFFL_GetViewModes(formptr);
					ns.Type   = CUSTOMSCREEN|SCREENQUIET|SCREENBEHIND;

					if((s1 = OpenScreen(&ns)) && (s2 = OpenScreen(&ns)))
					{
						count = IFFL_GetColorTab(formptr,(WORD *)palette);
						if(count>32) count = 32;
						LoadRGB4(&s1->ViewPort,palette,count);
						LoadRGB4(&s2->ViewPort,palette,count);

						if(IFFL_DecodePic(formptr,&s1->BitMap))
						{
							ClearAll();
							ScreenToFront(s1);
							IFFL_DecodePic(formptr,&s2->BitMap);
							formptr = IFFL_FindChunk(formptr,0L);	/* 1st DLTA */
							if(GetNextBob(&s1->BitMap))
							{
								while(*formptr==ID_FORM)
								{
									LONG tmp;
									if(!IFFL_ModifyFrame(formptr,&s2->BitMap))
									{
										ShowIFFError("Bad ANIM format");
										break;
									}
									tmp=(LONG)s2; s2=s1; s1=(struct Screen *)tmp;
									ScreenToFront(s1);
									if(!GetNextBob(&(s1->BitMap))) break;
									formptr=IFFL_FindChunk(formptr,0L);
								}
							}
							ScreenToFront(mainscreen);
						}
						else ShowIFFError("Error decoding picture");
					}
					else ShowMonoReq2("Can't open anim screens");
					if(s1) CloseScreen(s1);
					if(s2) CloseScreen(s2);
				}
				else ShowIFFError("Mangled IFF ANIM");
			}
			else
			{
				char buf[150];
				sprintf(buf,"'%s':\nNot an IFF ANIM file!",path);
				ShowMonoReq2(buf);
			}
			IFFL_CloseIFF(ifffile);
		}
		else ShowIFFError(path);

	}	/* if(FileRequest()) */

	UnLockWindows();
}

