#include <libraries/iff.h>
#include "Bobi.h"

void ShowIFFError(char *text1)
{
	char	buf[250];
	char	*text2;

	switch(IFFL_IFFError())
	{
		case IFFL_ERROR_OPEN:			text2 = "Can't open file";
										break;

		case IFFL_ERROR_READ:			text2 = "Read error";
										break;

		case IFFL_ERROR_NOMEM:			text2 = "Not enough memory";
										break;

		case IFFL_ERROR_NOTIFF:			text2 = "Not an IFF file";
										break;

		case IFFL_ERROR_WRITE:			text2 = "Error writing file";
										break;

		case IFFL_ERROR_NOILBM:			text2 = "Not a picture";
										break;

		case IFFL_ERROR_NOBMHD:			text2 = "No BitMapHeader found";
										break;

		case IFFL_ERROR_NOBODY:			text2 = "No BODY chunk found";
										break;

		case IFFL_ERROR_BADCOMPRESSION:	text2 = "Unknown compression type";
										break;

		case IFFL_ERROR_NOANHD:			text2 = "No ANHD chunk found";
										break;

		case IFFL_ERROR_NODLTA:			text2 = "No DLTA chunk found";
										break;

		default:						text2 = "Unknown IFF error";
										break;
	}

	sprintf(buf,"%s:\n%s!",text1,text2); ShowMonoReq2(buf);
}

