#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <graphics/rastport.h>

#include "Bobi.h"
#include "BobStructure.h"

extern struct MyBob *BobTable[];
extern struct Screen *mainscreen;
extern struct Window *mainwindow,*toolwindow;
extern struct RastPort *mainrastport;
extern WORD numbobs,actbobnum,options,mainx0,mainy0;
extern struct Gadget AutoSizeGadget;
extern char LabelGadgetSIBuff[];
extern struct MenuItem CollisionRectMenuItem;

/************************************************************************/

static void DrawBobCross(struct MyBob *bob,WORD x,WORD y)
{
	register struct RastPort *rp=mainrastport;
	register WORD xl,yl;

	xl = mainx0-bob->X0;
	yl = mainy0-bob->Y0;

	SetDrMd(rp,COMPLEMENT);
	Move(rp,xl,y);
	Draw(rp,xl+bob->Width-1,y);
	Move(rp,x,yl);
	Draw(rp,x,yl+bob->Height-1);
}

/************************************************************************/

void SetOrgFunc()
{
	register struct MyBob *bob;
	register struct IntuiMessage *msg;
	register BOOL looping=TRUE;
	register ULONG oldidcmp;
	register WORD x,y;

	if((numbobs>0) && (numbobs!=actbobnum))
	{
		bob = BobTable[actbobnum];
		x = mainx0;
		y = mainy0;
		DrawBobCross(bob,x,y);
		oldidcmp = mainwindow->IDCMPFlags;
		mainwindow->Flags |= RMBTRAP;
		ModifyIDCMP(mainwindow,MOUSEBUTTONS);
		do
		{
			WaitTOF();
			if(msg = (struct IntuiMessage *)GetMsg(mainwindow->UserPort))
			{
				switch(msg->Code)
				{
					case SELECTDOWN:
						if(!BobHit(bob,msg->MouseX,msg->MouseY))
						{
							bob->X0 = x-mainx0+bob->X0;
							bob->Y0 = y-mainy0+bob->Y0;
						}
												/* hier kein break! */
					case MENUDOWN:
						looping=FALSE;
						break;
				}
			}
			else if(!BobHit(bob,mainwindow->MouseX,mainwindow->MouseY))
			{
				DrawBobCross(bob,x,y);
				x = mainwindow->MouseX;
				y = mainwindow->MouseY;
				DrawBobCross(bob,x,y);
			}
		} while(looping);

		DrawBobCross(bob,x,y);
		mainwindow->Flags &= ~RMBTRAP;
		ModifyIDCMP(mainwindow,oldidcmp);
	}
	else ShowMonoReq2("Please select a bob first!");
}

/************************************************************************/

void SetMainOrgFunc()
{
	register ULONG oldidcmp;
	register WORD x,y;

	x=mainx0; y=mainy0;
	DrawCross(mainscreen,x,y);

	oldidcmp = mainwindow->IDCMPFlags;
	mainwindow->Flags |= RMBTRAP;
	ModifyIDCMP(mainwindow,MOUSEBUTTONS);
	DrawCross(mainscreen,mainx0,mainy0); /* Am alten Ort zur Orientierung */
	for(;;)
	{
		register struct IntuiMessage *msg;

		WaitTOF();
		if(msg = (struct IntuiMessage *)GetMsg(mainwindow->UserPort))
		{
			DrawCross(mainscreen,mainx0,mainy0);
			if(msg->Code==SELECTDOWN)
			{
				mainx0=x;	/* Werte nur übernehmen wenn linke Maustaste */
				mainy0=y;
			}
			DrawCross(mainscreen,x,y);
			ReplyMsg((struct Message *)msg);
			break;
		}
		else
		{
			DrawCross(mainscreen,x,y);
			x = mainwindow->MouseX;
			y = mainwindow->MouseY;
			DrawCross(mainscreen,x,y);
		}
	}
	mainwindow->Flags &= ~RMBTRAP;
	ModifyIDCMP(mainwindow,oldidcmp);
}

/************************************************************************/

void DragBobFunc()
{
	WORD xold,yold,dx,dy;
	register struct MyBob *bob;
	register struct IntuiMessage *msg;
	register struct BitMap *tmpmap;
	register WORD x,y,xoff,yoff;
	BOOL looping=TRUE;
	LONG oldidcmp;

	if((numbobs>0) && (numbobs!=actbobnum))
	{
		bob = BobTable[actbobnum];
		if(tmpmap=MakeBitMap(bob->Width,bob->Height,bob->Depth))
		{
			CopyMem(bob->Planes[0],tmpmap->Planes[0],bob->PlaneSize*bob->Depth);

			xoff = mainwindow->MouseX+bob->X0-mainx0;
			yoff = mainwindow->MouseY+bob->Y0-mainy0;
			x = xold = mainwindow->MouseX-xoff;
			y = yold = mainwindow->MouseY-yoff;
			oldidcmp = mainwindow->IDCMPFlags;
			mainwindow->Flags |= RMBTRAP;
			ModifyIDCMP(mainwindow,MOUSEBUTTONS);
			DrawRect(mainrastport,(WORD)(x-1),(WORD)(y-1),(WORD)(x+bob->Width),
				(WORD)(y+bob->Height),0);	/* Macht SetAPen,0 */
			do
			{
				WaitTOF();
				msg=(struct IntuiMessage *)GetMsg(mainwindow->UserPort);
				if(toolwindow) if(!msg)
					msg=(struct IntuiMessage *)GetMsg(toolwindow->UserPort);
				if(msg)
				{
					switch(msg->Code)
					{
						case SELECTUP:
							bob->X0 = mainx0-x;
							bob->Y0 = mainy0-y;
							looping=FALSE;
							break;

						case MENUDOWN:
							RectFill(mainrastport,x-1,y-1,x+bob->Width,
							   y+bob->Height);
							looping=FALSE;
							break;
					}
					ReplyMsg((struct Message *)msg);
				}
				else
				{
					xold=x; yold=y;
					x = mainwindow->MouseX-xoff;
					y = mainwindow->MouseY-yoff;
		/*			if(x<MINX) x = MINX;
					if(y<MINY) y = MINY;
					if(x > MAXX-bob->Width)  x = MAXX-bob->Width;
					if(y > MAXY-bob->Height) y = MAXY-bob->Height;
		*/
					dx = x-xold; dy = y-yold;

					if(dx>0)
						RectFill(mainrastport,xold-1,yold-1,x-1,yold+bob->Height);
					else
						RectFill(mainrastport,x+bob->Width,yold-1,xold+bob->Width,yold+bob->Height);

					if(dy>0)
						RectFill(mainrastport,xold-1,yold-1,xold+bob->Width,y);
					else
						RectFill(mainrastport,xold-1,y+bob->Height,xold+bob->Width,yold+bob->Height);

					WaitTOF();
					BltBitMapRastPort(tmpmap,0,0,mainrastport,x,y,bob->Width,bob->Height,0xc0);
					WaitBlit();
				}
			} while(looping);
			mainwindow->Flags &= ~RMBTRAP;
			ModifyIDCMP(mainwindow,oldidcmp);
			MyFreeBitMap(tmpmap);
		}
		else ShowMonoReq2("Not enough memory to move bob!");
	}
	else ShowMonoReq2("Internal Error 193 in Layer.c !");
}

/************************************************************************/

void SetCollBoundsFunc()
{
	register struct MyBob *bob;
	register struct IntuiMessage *msg;
	register BOOL looping=TRUE;
	register ULONG oldidcmp;
	register WORD x,y;

	if((numbobs>0) && (numbobs!=actbobnum))
	{
		CollisionRectMenuItem.Flags |= CHECKED;	/* Enable collision */
		options |= GOF_COLLISIONRECT;

		bob = BobTable[actbobnum];
		x = mainx0-bob->X0+bob->CollX0;
		y = mainy0-bob->Y0+bob->CollY0;
		DrawCross(mainscreen,x,y);

		oldidcmp = mainwindow->IDCMPFlags;
		mainwindow->Flags |= RMBTRAP;
		ModifyIDCMP(mainwindow,MOUSEBUTTONS);
		do
		{
			WaitTOF();
			if(msg = (struct IntuiMessage *)GetMsg(mainwindow->UserPort))
			{
				switch(msg->Code)
				{
				case SELECTDOWN:
					/* if(!BobHit(bob,msg->MouseX,msg->MouseY)) */
					{
						DrawCross(mainscreen,x,y);
						bob->CollX0 = x+bob->X0-mainx0;
						bob->CollY0 = y+bob->Y0-mainy0;
						ShowBob(bob);
						x = mainx0-bob->X0+bob->CollX1;
						y = mainy0-bob->Y0+bob->CollY1;
						DrawCross(mainscreen,x,y);
					}
					break;

				case SELECTUP:
					/* if(!BobHit(bob,msg->MouseX,msg->MouseY)) */
					{
						DrawCross(mainscreen,x,y);
						bob->CollX1 = x+bob->X0-mainx0;
						bob->CollY1 = y+bob->Y0-mainy0;
					}
					looping=FALSE;
					break;
				}
				ReplyMsg((struct Message *)msg);
			}
			else /* if(!BobHit(bob,mainwindow->MouseX,mainwindow->MouseY)) */
			{
				DrawCross(mainscreen,x,y);
				x = mainwindow->MouseX;
				y = mainwindow->MouseY;
				DrawCross(mainscreen,x,y);
			}
		} while(looping);

		mainwindow->Flags &= ~RMBTRAP;
		ModifyIDCMP(mainwindow,oldidcmp);
	}
	else ShowMonoReq2("Please select a bob first!");
}

