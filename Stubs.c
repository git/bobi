#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include "Bobi.h"

extern struct NewWindow ToolNW;
extern struct Screen *mainscreen;
extern struct Window *mainwindow,*toolwindow;
extern struct RastPort *toolrastport;
extern WORD mainpalette[];
extern struct MenuItem ToolWindowMenuItem;

/*************************************************************************/

void ClearAllFunc()
{
	if(ShowRequest2("Do you really want to\ndelete all bobs?","OF COURSE!"))
		ClearAll();
}

/*************************************************************************/

void QuitFunc()
{
	if(ShowRequest2("Do you really want to quit ?","JAJA!"))
		Cleanup(TRUE);
	else ShowMonoReq2("Da hast du nochmals Glück gehabt!");
}

/*************************************************************************/

void GrabDPaintFunc()
{
	ShowMonoReq2("Can't find the DPaint screen :-)");
}

/*************************************************************************/

void GrabScreenFunc()
{
	ShowMonoReq2("Sorry, this function will\nnever be implemented!");
}

/*************************************************************************/

void SaveIFFAnimFunc()
{
	ShowMonoReq2("Sorry, this function is not\nyet implemented!");
}

/*************************************************************************/

void EditPaletteFunc()
{
	Snooze(mainwindow);		/* nicht LockWindows() wegen Farbpalette! */
	if(toolwindow) Snooze(toolwindow);
	CopyMem(RequestColor(mainscreen),mainpalette,2*32);
	if(toolwindow) UnSnooze(toolwindow);
	UnSnooze(mainwindow);
}

/*************************************************************************/

void ToolWindowFunc()
{
	if(ToolWindowMenuItem.Flags & CHECKED)
	{
		if(!toolwindow)
		{
			ToolNW.Screen  = mainscreen;
			if(ToolNW.TopEdge>mainscreen->Height-ToolNW.Height)
				ToolNW.TopEdge=mainscreen->Height-ToolNW.Height;
			if(!(toolwindow = OpenWindow(&ToolNW)))
			{
				ShowMonoReq2("Can't open tool window!\n");
			}
			toolrastport = toolwindow->RPort;
		}
	}
	else
	{
		if(toolwindow)
		{
			ToolNW.TopEdge=toolwindow->TopEdge;
			ClearMenuStrip(toolwindow);
			CloseWindow(toolwindow);
			toolwindow=0; toolrastport=0;
			ActivateWindow(mainwindow);
		}
	}
}

