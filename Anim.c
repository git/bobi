/*
**  Bobi - The Ultimate Amiga Bob Manipulator
**
**  Anim.c - Bobs animieren
**
**  COPYRIGHT (C) 1989-1993 BY CHRISTIAN A. WEBER, ZUERICH, SWITZERLAND.
**  ALL RIGHTS RESERVED. NO PART OF THIS SOFTWARE MAY BE COPIED, REPRODUCED,
**  OR TRANSMITTED IN ANY FORM OR BY ANY MEANS, WITHOUT THE PRIOR WRITTEN
**  PERMISSION OF THE AUTHOR. NO WARRANTY. USE AT YOUR OWN RISK.
*/

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include "Bobi.h"
#include "BobStructure.h"

#define MAXANIMDELAY 50

extern struct Gadget AnimSpeedGadget;
extern struct PropInfo AnimSpeedGadgetSInfo;
extern struct MyBob *BobTable[];
extern struct Window *mainwindow,*toolwindow;
extern WORD numbobs,actbobnum;
extern WORD firstanimbob,lastanimbob,animflags;

extern struct MenuItem ForwardItem,BackwardItem,PingPongItem;


/*************************************************************************/

void SetFirstBobFunc()
{
	if(BobTable[actbobnum]) firstanimbob = actbobnum;
	else                    ShowMonoReq2("No bob selected!");
}


/*************************************************************************/

void SetLastBobFunc()
{
	if(BobTable[actbobnum]) lastanimbob = actbobnum;
	else                    ShowMonoReq2("No bob selected!");
}


/*************************************************************************/

void StartAnimFunc()
{
	register struct IntuiMessage *msg;
	register LONG bobnum,dir,delay,i;
	struct Menu *mainmenus,*toolmenus;
	STRPTR oldmaintitle,oldtooltitle;

	oldmaintitle=mainwindow->ScreenTitle;
	SetWindowTitles(mainwindow,(STRPTR)(~0L),"Hit <ESC> or RMB to stop animation");
	mainmenus = mainwindow->MenuStrip;
	ClearMenuStrip(mainwindow);

	if(toolwindow)
	{
		oldtooltitle=toolwindow->ScreenTitle;
		SetWindowTitles(toolwindow,(STRPTR)(~0L),"Hit <ESC> or RMB to stop animation");
		toolmenus = toolwindow->MenuStrip;
		ClearMenuStrip(toolwindow);
	}

	if(lastanimbob < firstanimbob)
	{
		bobnum = lastanimbob;
		lastanimbob = firstanimbob;
		firstanimbob = bobnum;
	}
	bobnum = firstanimbob;
	dir=1; if(animflags & AF_BACKWARD) dir=-1;

	for(;;)
	{
		delay = (GetAnimSpeed()*MAXANIMDELAY+0x7fffL)>>16;
		for(i=0; i<delay; ++i)
		{
			WaitTOF();
			msg=(struct IntuiMessage *)GetMsg(mainwindow->UserPort);
			if(toolwindow)
			{
				if(!msg) msg=(struct IntuiMessage *)GetMsg(toolwindow->UserPort);
			}

			if(msg)
			{
				if((msg->Class==MENUPICK) ||
				   ((msg->Class==RAWKEY)     && (msg->Code==0x45)))
				{
					ReplyMsg((struct Message *)msg);
					goto endanim;
				}
				ReplyMsg((struct Message *)msg);
			}
		}
		bobnum += dir;
		if(animflags & AF_BOUNCE)
		{
			if((bobnum>lastanimbob) || (bobnum<firstanimbob))
			{
				dir = (-dir);
				bobnum += dir;
				bobnum += dir;
			}
		}
		else
		{
			if(bobnum>lastanimbob)  bobnum = firstanimbob;
			if(bobnum<firstanimbob) bobnum = lastanimbob;
		}
		ShowBob(BobTable[bobnum]);
	}
endanim:
	actbobnum = bobnum;
	SetWindowTitles(mainwindow,(STRPTR)(~0L),oldmaintitle);
	if(toolwindow) SetWindowTitles(toolwindow,(STRPTR)(~0L),oldtooltitle);
	ShowFrame(actbobnum);
	SetMenuStrip(mainwindow,mainmenus);
	if(toolwindow) SetMenuStrip(toolwindow,toolmenus);
}


/*************************************************************************/

UWORD GetAnimSpeed()
{
	return(AnimSpeedGadgetSInfo.VertPot);
}


/*************************************************************************/

void SetAnimSpeed(UWORD speed)
{
	AnimSpeedGadgetSInfo.VertBody = 0xffffL/MAXANIMDELAY;
	AnimSpeedGadgetSInfo.VertPot  = speed;

	if(toolwindow)
		RefreshGList(&AnimSpeedGadget,toolwindow,0,1);
}


/*************************************************************************/

void SetAnimFlags(WORD flags)
{
	ForwardItem.MutualExclude  = 6;
	BackwardItem.MutualExclude = 5;
	PingPongItem.MutualExclude = 3;

	ForwardItem.Flags  &= ~CHECKED;
	BackwardItem.Flags &= ~CHECKED;
	PingPongItem.Flags &= ~CHECKED;

	if(flags & AF_FORWARD)
	{
		ForwardItem.Flags  |= CHECKED;
	}
	if(flags & AF_BACKWARD)
	{
		BackwardItem.Flags |= CHECKED;
	}
	if(flags & AF_BOUNCE)
	{
		PingPongItem.Flags |= CHECKED;
	}

	animflags = flags;
}


/*************************************************************************/

void SetAnimModeFunc()
{
	if(ForwardItem.Flags & CHECKED)
	{
		animflags = AF_FORWARD;
	}
	else if(BackwardItem.Flags & CHECKED)
	{
		animflags = 0;
	}
	else /* if(PingPongItem.Flags & CHECKED) */
	{
		animflags = AF_BOUNCE;
	}
}
