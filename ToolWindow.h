
static UBYTE UNDOBUFFER[64];

static SHORT BorderVectors4[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
static struct Border Border4 = {
	135,-23280,
	1,0,JAM1,
	5,
	BorderVectors4,
	NULL
};

static SHORT BorderVectors3[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
static struct Border Border3 = {
	95,-17624,
	1,0,JAM1,
	5,
	BorderVectors3,
	&Border4
};

static SHORT BorderVectors2[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
static struct Border Border2 = {
	175,-3208,
	1,0,JAM1,
	5,
	BorderVectors2,
	&Border3
};

static SHORT BorderVectors1[] = {
	0,0,
	37,0,
	37,10,
	0,10,
	0,0
};
static struct Border Border1 = {
	215,-3072,
	1,0,JAM1,
	5,
	BorderVectors1,
	&Border2
};

static struct Gadget Gadget8 = {
	NULL,
	0,0,
	1,1,
	GADGHBOX+GADGHIMAGE,
	NULL,
	BOOLGADGET,
	(APTR)&Border1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

static UBYTE LabelGadgetSIBuff[64] =
	"abcdefghhhhhhhhhhhk";
static struct StringInfo LabelGadgetSInfo = {
	LabelGadgetSIBuff,
	UNDOBUFFER,
	0,
	64,
	0,
	0,0,0,0,0,
	0,
	0,
	NULL
};

static SHORT BorderVectors5[] = {
	0,0,
	155,0,
	155,10,
	0,10,
	0,1
};
static struct Border Border5 = {
	-1,-2,
	3,0,JAM1,
	5,
	BorderVectors5,
	NULL
};

static struct IntuiText IText1 = {
	3,0,JAM2,
	28,-11,
	NULL,
	"Source Label:",
	NULL
};

static struct Gadget LabelGadget = {
	&Gadget8,
	96,-13,
	154,9,
	GRELBOTTOM,
	RELVERIFY,
	STRGADGET,
	(APTR)&Border5,
	NULL,
	&IText1,
	NULL,
	(APTR)&LabelGadgetSInfo,
	NULL,
	NULL
};

static SHORT BorderVectors6[] = {
	0,0,
	33,0,
	33,21,
	0,21,
	0,0
};
static struct Border Border6 = {
	-1,-1,
	3,0,JAM1,
	5,
	BorderVectors6,
	NULL
};

static struct IntuiText IText3 = {
	1,0,JAM1,
	4,11,
	NULL,
	"Org",
	NULL
};

static struct IntuiText IText2 = {
	1,0,JAM1,
	4,2,
	NULL,
	"Set",
	&IText3
};

static struct Gadget Gadget6 = {
	&LabelGadget,
	257,-25,
	32,20,
	GRELBOTTOM,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border6,
	NULL,
	&IText2,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors7[] = {
	0,0,
	33,0,
	33,21,
	0,21,
	0,0
};
static struct Border Border7 = {
	-1,-1,
	3,0,JAM1,
	5,
	BorderVectors7,
	NULL
};

static struct IntuiText IText5 = {
	1,0,JAM1,
	4,11,
	NULL,
	"Bob",
	NULL
};

static struct IntuiText IText4 = {
	1,0,JAM1,
	4,2,
	NULL,
	"Get",
	&IText5
};

static struct Gadget Gadget5 = {
	&Gadget6,
	257,-50,
	32,20,
	GRELBOTTOM,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border7,
	NULL,
	&IText4,
	NULL,
	NULL,
	NULL,
	NULL
};

static struct PropInfo AnimSpeedGadgetSInfo = {
	AUTOKNOB+FREEVERT,
	-1,20755,
	-1,4095,
};

static struct Image Image1 = {
	0,12,
	6,4,
	0,
	NULL,
	0x0000,0x0000,
	NULL
};

static struct Gadget AnimSpeedGadget = {
	&Gadget5,
	4,-51,
	14,47,
	GRELBOTTOM,
	NULL,
	PROPGADGET,
	(APTR)&Image1,
	NULL,
	NULL,
	NULL,
	(APTR)&AnimSpeedGadgetSInfo,
	NULL,
	NULL
};

static SHORT BorderVectors8[] = {
	0,0,
	71,0,
	71,11,
	0,11,
	0,0
};
static struct Border Border8 = {
	-1,-1,
	3,0,JAM1,
	5,
	BorderVectors8,
	NULL
};

static struct IntuiText IText6 = {
	1,0,JAM1,
	4,1,
	NULL,
	"Anim Key",
	NULL
};

static struct Gadget AnimKeyGadget = {
	&AnimSpeedGadget,
	21,-15,
	70,10,
	GRELBOTTOM+SELECTED,
	RELVERIFY+TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border8,
	NULL,
	&IText6,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors9[] = {
	0,0,
	71,0,
	71,11,
	0,11,
	0,0
};
static struct Border Border9 = {
	-1,-1,
	3,0,JAM1,
	5,
	BorderVectors9,
	NULL
};

static struct IntuiText IText7 = {
	1,0,JAM1,
	4,1,
	NULL,
	"Auto Org",
	NULL
};

static struct Gadget AutoOrgGadget = {
	&AnimKeyGadget,
	21,-33,
	70,10,
	GRELBOTTOM+SELECTED,
	RELVERIFY+TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border9,
	NULL,
	&IText7,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors10[] = {
	0,0,
	71,0,
	71,11,
	0,11,
	0,0
};
static struct Border Border10 = {
	-1,-1,
	3,0,JAM1,
	5,
	BorderVectors10,
	NULL
};

static struct IntuiText IText8 = {
	1,0,JAM1,
	4,1,
	NULL,
	"AutoSize",
	NULL
};

static struct Gadget AutoSizeGadget = {
	&AutoOrgGadget,
	21,-50,
	70,10,
	GRELBOTTOM+SELECTED,
	RELVERIFY+TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border10,
	NULL,
	&IText8,
	NULL,
	NULL,
	NULL,
	NULL
};

#define GadgetList1 AutoSizeGadget

static struct NewWindow NewWindowStructure1 = {
	0,61,
	295,65,
	0,1,
	MOUSEBUTTONS+GADGETDOWN+GADGETUP+MENUPICK+RAWKEY,
	WINDOWDRAG+WINDOWCLOSE+REPORTMOUSE+ACTIVATE+NOCAREREFRESH,
	&AutoSizeGadget,
	NULL,
	"Bobi Tool Window",
	NULL,
	NULL,
	0,0,
	0,0,
	CUSTOMSCREEN
};


void HandleEvent(object)
APTR object;
{
  if (object == (APTR)&Gadget5) { GetBobFunc(      ); return; }
  if (object == (APTR)&Gadget6) { SetOrgFunc(      ); return; }
}
#define HANDLEEVENT HandleEvent

/* end of PowerWindows source generation */
