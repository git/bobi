
static UBYTE UNDOBUFFER[80];

static SHORT BorderVectors3[] = {
	0,0,
	67,0,
	67,86,
	0,86,
	0,0
};
static struct Border Border3 = {
	138,12,
	3,0,JAM1,
	5,
	BorderVectors3,
	NULL
};

static SHORT BorderVectors2[] = {
	0,0,
	61,0,
	61,86,
	0,86,
	0,0
};
static struct Border Border2 = {
	4,12,
	3,0,JAM1,
	5,
	BorderVectors2,
	&Border3
};

static SHORT BorderVectors1[] = {
	0,0,
	67,0,
	67,86,
	0,86,
	0,0
};
static struct Border Border1 = {
	68,12,
	3,0,JAM1,
	5,
	BorderVectors1,
	&Border2
};

static struct Gadget Gadget13 = {
	NULL,
	0,0,
	1,1,
	GADGHBOX+GADGHIMAGE,
	NULL,
	BOOLGADGET,
	(APTR)&Border1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors4[] = {
	0,0,
	99,0,
	99,14,
	0,14,
	0,0
};
static struct Border Border4 = {
	-1,-1,
	31,0,JAM1,
	5,
	BorderVectors4,
	NULL
};

static struct IntuiText IText1 = {
	1,0,JAM1,
	26,3,
	NULL,
	"CANCEL",
	NULL
};

static struct Gadget CancelGadget = {
	&Gadget13,
	-102,-16,
	98,13,
	GRELBOTTOM+GRELRIGHT,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border4,
	NULL,
	&IText1,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors5[] = {
	0,0,
	99,0,
	99,14,
	0,14,
	0,0
};
static struct Border Border5 = {
	-1,-1,
	31,0,JAM1,
	5,
	BorderVectors5,
	NULL
};

static struct IntuiText IText2 = {
	1,0,JAM1,
	26,3,
	NULL,
	"Do it!",
	NULL
};

static struct Gadget OKGadget = {
	&CancelGadget,
	5,-16,
	98,13,
	GRELBOTTOM,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border5,
	NULL,
	&IText2,
	NULL,
	NULL,
	NULL,
	NULL
};

static UBYTE FileNameGadgetSIBuff[80] =
	"RAM:BobTest.o";
static struct StringInfo FileNameGadgetSInfo = {
	FileNameGadgetSIBuff,
	UNDOBUFFER,
	0,
	80,
	0,
	0,0,0,0,0,
	0,
	0,
	NULL
};

static SHORT BorderVectors6[] = {
	0,0,
	201,0,
	201,20,
	0,20,
	0,1
};
static struct Border Border6 = {
	-1,-11,
	3,0,JAM1,
	5,
	BorderVectors6,
	NULL
};

static struct Gadget FileNameGadget = {
	&OKGadget,
	5,135,
	200,9,
	SELECTED,
	RELVERIFY,
	STRGADGET,
	(APTR)&Border6,
	NULL,
	NULL,
	NULL,
	(APTR)&FileNameGadgetSInfo,
	NULL,
	NULL
};

static UBYTE SectionNameGadgetSIBuff[80] =
	"BobTestData";
static struct StringInfo SectionNameGadgetSInfo = {
	SectionNameGadgetSIBuff,
	UNDOBUFFER,
	0,
	80,
	0,
	0,0,0,0,0,
	0,
	0,
	NULL
};

static SHORT BorderVectors7[] = {
	0,0,
	201,0,
	201,20,
	0,20,
	0,1
};
static struct Border Border7 = {
	-1,-11,
	3,0,JAM1,
	5,
	BorderVectors7,
	NULL
};

static struct Gadget SectionNameGadget = {
	&FileNameGadget,
	5,112,
	200,9,
	NULL,
	RELVERIFY,
	STRGADGET,
	(APTR)&Border7,
	NULL,
	NULL,
	NULL,
	(APTR)&SectionNameGadgetSInfo,
	NULL,
	NULL
};

static SHORT BorderVectors8[] = {
	0,0,
	57,0,
	57,21,
	0,21,
	0,0
};
static struct Border Border8 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors8,
	NULL
};

static struct IntuiText IText4 = {
	1,0,JAM1,
	12,11,
	NULL,
	"Mask",
	NULL
};

static struct IntuiText IText3 = {
	1,0,JAM1,
	16,2,
	NULL,
	"Bob",
	&IText4
};

static struct Gadget MaskGadget = {
	&SectionNameGadget,
	144,75,
	56,20,
	NULL,
	TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border8,
	NULL,
	&IText3,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors9[] = {
	0,0,
	57,0,
	57,21,
	0,21,
	0,0
};
static struct Border Border9 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors9,
	NULL
};

static struct IntuiText IText6 = {
	1,0,JAM1,
	12,11,
	NULL,
	"Data",
	NULL
};

static struct IntuiText IText5 = {
	1,0,JAM1,
	16,2,
	NULL,
	"Bob",
	&IText6
};

static struct Gadget BobDataGadget = {
	&MaskGadget,
	144,51,
	56,20,
	NULL,
	TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border9,
	NULL,
	&IText5,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors10[] = {
	0,0,
	57,0,
	57,21,
	0,21,
	0,0
};
static struct Border Border10 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors10,
	NULL
};

static struct IntuiText IText8 = {
	1,0,JAM1,
	8,11,
	NULL,
	"Table",
	NULL
};

static struct IntuiText IText7 = {
	1,0,JAM1,
	8,2,
	NULL,
	"Color",
	&IText8
};

static struct Gadget ColorGadget = {
	&BobDataGadget,
	144,27,
	56,20,
	NULL,
	TOGGLESELECT,
	BOOLGADGET,
	(APTR)&Border10,
	NULL,
	&IText7,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors11[] = {
	0,0,
	57,0,
	57,21,
	0,21,
	0,0
};
static struct Border Border11 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors11,
	NULL
};

static struct IntuiText IText10 = {
	1,0,JAM1,
	4,11,
	NULL,
	"Module",
	NULL
};

static struct IntuiText IText9 = {
	1,0,JAM1,
	4,2,
	NULL,
	"Object",
	&IText10
};

static struct Gadget ObjectGadget = {
	&ColorGadget,
	74,51,
	56,20,
	NULL,
	GADGIMMEDIATE,
	BOOLGADGET,
	(APTR)&Border11,
	NULL,
	&IText9,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors12[] = {
	0,0,
	57,0,
	57,21,
	0,21,
	0,0
};
static struct Border Border12 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors12,
	NULL
};

static struct IntuiText IText12 = {
	1,0,JAM1,
	12,11,
	NULL,
	"Data",
	NULL
};

static struct IntuiText IText11 = {
	1,0,JAM1,
	16,2,
	NULL,
	"Raw",
	&IText12
};

static struct Gadget RawDataGadget = {
	&ObjectGadget,
	74,75,
	56,20,
	NULL,
	GADGIMMEDIATE,
	BOOLGADGET,
	(APTR)&Border12,
	NULL,
	&IText11,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors13[] = {
	0,0,
	57,0,
	57,21,
	0,21,
	0,0
};
static struct Border Border13 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors13,
	NULL
};

static struct IntuiText IText14 = {
	1,0,JAM1,
	4,11,
	NULL,
	"Source",
	NULL
};

static struct IntuiText IText13 = {
	1,0,JAM1,
	8,2,
	NULL,
	"Assem",
	&IText14
};

static struct Gadget AssemGadget = {
	&RawDataGadget,
	74,27,
	56,20,
	NULL,
	GADGIMMEDIATE,
	BOOLGADGET,
	(APTR)&Border13,
	NULL,
	&IText13,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors14[] = {
	0,0,
	53,0,
	53,33,
	0,33,
	0,0
};
static struct Border Border14 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors14,
	NULL
};

static struct IntuiText IText15 = {
	1,0,JAM1,
	2,12,
	NULL,
	"Sprite",
	NULL
};

static struct Gadget GenerateSpritesGadget = {
	&AssemGadget,
	9,63,
	52,32,
	NULL,
	GADGIMMEDIATE,
	BOOLGADGET,
	(APTR)&Border14,
	NULL,
	&IText15,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors15[] = {
	0,0,
	53,0,
	53,33,
	0,33,
	0,0
};
static struct Border Border15 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors15,
	NULL
};

static struct IntuiText IText16 = {
	1,0,JAM1,
	14,12,
	NULL,
	"Bob",
	NULL
};

static struct Gadget GenerateBobsGadget = {
	&GenerateSpritesGadget,
	9,27,
	52,32,
	NULL,
	GADGIMMEDIATE,
	BOOLGADGET,
	(APTR)&Border15,
	NULL,
	&IText16,
	NULL,
	NULL,
	NULL,
	NULL
};

#define GadgetList1 GenerateBobsGadget

static struct IntuiText IText21 = {
	3,0,JAM1,
	83,15,
	NULL,
	"Mode:",
	NULL
};

static struct IntuiText IText20 = {
	3,0,JAM1,
	53,104,
	NULL,
	"Section Name:",
	&IText21
};

static struct IntuiText IText19 = {
	3,0,JAM1,
	141,15,
	NULL,
	"Options:",
	&IText20
};

static struct IntuiText IText18 = {
	3,0,JAM1,
	16,15,
	NULL,
	"Type:",
	&IText19
};

static struct IntuiText IText17 = {
	3,0,JAM1,
	69,127,
	NULL,
	"Filename:",
	&IText18
};

#define IntuiTextList1 IText17

static struct NewWindow NewWindowStructure1 = {
	50,21,
	210,165,
	0,1,
	GADGETDOWN+GADGETUP+CLOSEWINDOW,
	WINDOWDRAG+WINDOWCLOSE+ACTIVATE+RMBTRAP+NOCAREREFRESH,
	&GenerateBobsGadget,
	NULL,
	"     Generate Code      ",
	NULL,
	NULL,
	5,5,
	(UWORD)-1,(UWORD)-1,
	CUSTOMSCREEN
};
