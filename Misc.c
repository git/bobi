#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <stdarg.h>

#include "Bobi.h"

extern struct Screen	*mainscreen;
extern struct Window	*mainwindow,*toolwindow;
extern WORD				options;
extern UWORD			mainpalette[],Palette[];
extern BYTE				defaultorg;
extern struct MenuItem	LayerModeMenuItem,OrgGridMenuItem,
						BobBordersMenuItem,CollisionRectMenuItem,
						FirstDefaultOrgSubItem;
extern BYTE				arexxflag;

/****************************************************************************
**	Requester mit einem OK-Gadget anzeigen
*/

void ShowMonoReq2(char *text,...)
{
	char buf[256];
	va_list	args;

	va_start(args,text);
	vsprintf(buf,text,args);

	LockWindows();
	ShowRequest(buf,0," OK ",0x8000);
	UnLockWindows();
}

/****************************************************************************
**	Requester mit PosText und "Cancel" anzeigen
*/

BOOL ShowRequest2(char *text,char *postext)
{
	BOOL val;
	LockWindows();
	val = ShowRequest(text,postext,"CANCEL",0x8000);
	UnLockWindows();
	return(val);
}

/****************************************************************************
**	'Can't open file' plus Text ausgeben
*/

void ShowFileError(char *name)
{
	char buf[200];
	sprintf(buf,"Can't open file\n'%s' !",name);
	ShowMonoReq2(buf);
}

/****************************************************************************
**	File zum Schreiben öffnen und Requester bringen falls es schon existiert
*/

BPTR OpenNewFileSafely(char *name)
{
	register BPTR file;
	char buf[200];

	if(!arexxflag)		/* Wenn das Kommando von ARexx kommt wird nix getestet */
	{
		if(file=Lock(name,ACCESS_READ))
		{
			UnLock(file);
			sprintf(buf,"Warning: The file\n'%s'\nexists. Save over top of it?",name);
			if(!ShowRequest2(buf,"YES")) return 0;
		}
	}

	if(!(file=Open(name,MODE_NEWFILE)))
	{
		ShowFileError(name);
		return 0;
	}

	return file;
}

/****************************************************************************
**	Rahmen in gewünschter Farbe zeichnen
*/

void DrawRect(struct RastPort *rp,WORD x0,WORD y0,WORD x1,WORD y1,WORD col)
{
	SetDrMd(rp,JAM1);
	SetAPen(rp,col);
	Move(rp,x0,y0);
	Draw(rp,x1,y0);
	Draw(rp,x1,y1);
	Draw(rp,x0,y1);
	Draw(rp,x0,y0);
}

/****************************************************************************
**	Fadenkreuz zeichnen
*/

void DrawCross(struct Screen *screen,WORD x,WORD y)
{
	if(x>=0)
	{
		register struct RastPort *rp = &(screen->RastPort);
		SetDrMd(rp,COMPLEMENT);
		Move(rp,0,y);
		Draw(rp,screen->Width-1,y);
		Move(rp,x,0);
		Draw(rp,x,screen->Height-1);
	}
}

/****************************************************************************
**	MainScreen-Farbpalette setzen
*/

void LoadPalette(UWORD *pal)
{
	LoadRGB4(&(mainscreen->ViewPort),pal,32);
}

/****************************************************************************
**	Menus abschalten und Snooze-Pointer setzen
*/

static WORD LockCntr;

void LockWindows()
{
	if(!LockCntr)
	{
		LoadPalette(Palette);
		Snooze(mainwindow);
		if(toolwindow) Snooze(toolwindow);
	}
	LockCntr++;
}

/****************************************************************************
**	Windows wieder unlocken
*/

void UnLockWindows()
{
	if(LockCntr>0) --LockCntr;

	if(!LockCntr)
	{
		if(toolwindow) UnSnooze(toolwindow);
		UnSnooze(mainwindow);
		LoadPalette(mainpalette);
	}
}

/****************************************************************************
** Globale Optionen setzen und in den Menuitems updaten
*/

void SetGlobalOptions(WORD opts, BYTE deforg)
{
	struct MenuItem	*item;
	int				i;

	options = opts;
	if(opts & GOF_LAYERMODE)     LayerModeMenuItem.Flags  |=  CHECKED;
	else                         LayerModeMenuItem.Flags  &= ~CHECKED;
	if(opts & GOF_ORGGRID)       OrgGridMenuItem.Flags    |=  CHECKED;
	else                         OrgGridMenuItem.Flags    &= ~CHECKED;
	if(opts & GOF_BOBBORDERS)    BobBordersMenuItem.Flags |=  CHECKED;
	else                         BobBordersMenuItem.Flags &= ~CHECKED;
	if(opts & GOF_COLLISIONRECT) CollisionRectMenuItem.Flags |=  CHECKED;
	else                         CollisionRectMenuItem.Flags &= ~CHECKED;

	defaultorg = deforg;

	for(i=0,item = &FirstDefaultOrgSubItem; item; item=item->NextItem,++i)
	{
		if(i == defaultorg)		item->Flags |= CHECKED;
		else					item->Flags &= ~CHECKED;
	}
}

/****************************************************************************
**	Globale Optionen aus den Menuitems auslesen
*/

void GetGlobalOptions()
{
	struct MenuItem *item;

	options=0;
	if(LayerModeMenuItem.Flags     & CHECKED) options |= GOF_LAYERMODE;
	if(OrgGridMenuItem.Flags       & CHECKED) options |= GOF_ORGGRID;
	if(BobBordersMenuItem.Flags    & CHECKED) options |= GOF_BOBBORDERS;
	if(CollisionRectMenuItem.Flags & CHECKED) options |= GOF_COLLISIONRECT;

	defaultorg=0;
	for(item = &FirstDefaultOrgSubItem; item; item=item->NextItem)
	{
		if(item->Flags & CHECKED) break;
		defaultorg++;
	}
}

