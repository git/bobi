#include <proto/exec.h>
#include <proto/intuition.h>
#include <graphics/gfxbase.h>
#include <string.h>

#include "Bobi.h"

extern struct GfxBase	*GfxBase;
extern struct Screen	*mainscreen;
extern char				idstring[];
extern UWORD			mainpalette[];
extern ULONG			rexxsigmask;

/****************************************************************************
**	Screen und so schliessen und schlafen gehn
*/

void SleepFunc(void)
{
	struct Screen *wb;

	static struct NewWindow NewW =
	{
		-1,-1,-1,-1,0,1,CLOSEWINDOW,
		WINDOWDRAG+WINDOWDEPTH+WINDOWCLOSE+NOCAREREFRESH,
		NULL,NULL,idstring,NULL,NULL,0,0,(UWORD)-1,(UWORD)-1,WBENCHSCREEN
	};

	if(wb=(void *)OpenWorkBench())
	{
		struct Window *w;

		if(NewW.LeftEdge<0)
		{
			NewW.Width    = 85+8*strlen(idstring);
			NewW.Height   = wb->RastPort.TxHeight+3;
			NewW.LeftEdge = wb->Width-NewW.Width;
			NewW.TopEdge  = wb->Height-NewW.Height;
		}

		if(w=OpenWindow(&NewW))
		{
			Cleanup(FALSE);
			Wait(SIGMASK(w)|rexxsigmask);
			NewW.LeftEdge = w->LeftEdge;
			NewW.TopEdge  = w->TopEdge;
			CloseWindow(w);
			OpenMain();
			LoadPalette(mainpalette);
			RexxMsgHandler();
		}
		else ShowMonoReq2("Can't open sleep window!");
	}
	else ShowMonoReq2("Can't open the Workbench screen!");
}

