
static UBYTE FramesGadgetSIBuff[5] =
	"36";
static struct StringInfo FramesGadgetSInfo = {
	FramesGadgetSIBuff,
	NULL,
	0,
	5,
	0,
	0,0,0,0,0,
	0,
	36,
	NULL
};

static SHORT BorderVectors1[] = {
	0,0,
	174,0,
	174,12,
	0,12,
	0,1
};
static struct Border Border1 = {
	-1,-3,
	3,0,JAM1,
	5,
	BorderVectors1,
	NULL
};

static struct IntuiText IText1 = {
	3,0,JAM1,
	4,0,
	NULL,
	"# of Frames",
	NULL
};

static struct Gadget FramesGadget = {
	NULL,
	6,34,
	173,11,
	NULL,
	RELVERIFY+STRINGRIGHT+LONGINT,
	STRGADGET,
	(APTR)&Border1,
	NULL,
	&IText1,
	NULL,
	(APTR)&FramesGadgetSInfo,
	NULL,
	NULL
};

static UBYTE AngleGadgetSIBuff[5] =
	"360";
static struct StringInfo AngleGadgetSInfo = {
	AngleGadgetSIBuff,
	NULL,
	0,
	5,
	0,
	0,0,0,0,0,
	0,
	360,
	NULL
};

static SHORT BorderVectors2[] = {
	0,0,
	174,0,
	174,12,
	0,12,
	0,1
};
static struct Border Border2 = {
	-1,-3,
	3,0,JAM1,
	5,
	BorderVectors2,
	NULL
};

static struct IntuiText IText2 = {
	3,0,JAM1,
	4,0,
	NULL,
	"Angle (Total)",
	NULL
};

static struct Gadget AngleGadget = {
	&FramesGadget,
	6,17,
	173,11,
	NULL,
	RELVERIFY+STRINGRIGHT+LONGINT,
	STRGADGET,
	(APTR)&Border2,
	NULL,
	&IText2,
	NULL,
	(APTR)&AngleGadgetSInfo,
	NULL,
	NULL
};

static SHORT BorderVectors3[] = {
	0,0,
	81,0,
	81,12,
	0,12,
	0,0
};
static struct Border Border3 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors3,
	NULL
};

static struct IntuiText IText3 = {
	1,0,JAM1,
	16,2,
	NULL,
	"ROTATE",
	NULL
};

static struct Gadget OKGadget = {
	&AngleGadget,
	6,-15,
	80,11,
	GRELBOTTOM,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border3,
	NULL,
	&IText3,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors4[] = {
	0,0,
	81,0,
	81,12,
	0,12,
	0,0
};
static struct Border Border4 = {
	-1,-1,
	31,0,JAM1,
	5,
	BorderVectors4,
	NULL
};

static struct IntuiText IText4 = {
	31,0,JAM1,
	16,2,
	NULL,
	"CANCEL",
	NULL
};

static struct Gadget CancelGadget = {
	&OKGadget,
	-85,-15,
	80,11,
	GRELBOTTOM+GRELRIGHT,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border4,
	NULL,
	&IText4,
	NULL,
	NULL,
	NULL,
	NULL
};

#define GadgetList1 CancelGadget

static struct NewWindow NewWindowStructure1 = {
	63,39,
	185,70,
	0,1,
	GADGETUP+CLOSEWINDOW,
	WINDOWDRAG+WINDOWDEPTH+WINDOWCLOSE+ACTIVATE+NOCAREREFRESH,
	&CancelGadget,
	NULL,
	"   Rotate Bobs   ",
	NULL,
	NULL,
	5,5,
	(UWORD)-1,(UWORD)-1,
	CUSTOMSCREEN
};
