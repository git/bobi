/*************************************************************************
**                                                             			**
**   R E Q U E S T C O L O R                  		  Copyright : -C5-	**
**   -----------------------	                               			**
**                                                             			**
**   Der geniale Farbrequester     		                       			**
**                                                             			**
**   Created : 04-Mar-89 (-C5-)          Last Update: 16-Jul-89 (-C5-)	**
**                                                             			**
**************************************************************************
**                                                             			**
**   Format  : RequestColor(Screen)							   			**
**                                                             			**
*************************************************************************/


#include <proto/intuition.h>
#include <proto/graphics.h>
#include <graphics/gfx.h>
#include <proto/exec.h>
#include <proto/dos.h>


static void HandleEvent(APTR);
static void HandleColor(APTR);
static void CleanUp(void);
static void Exchange(APTR);
static void Spread(APTR);
static void Default(APTR);
static void SelectColor(APTR);
static void EndColor(APTR);
static void QuitColor(APTR);
static void Undo(APTR);
static void CopyColor(APTR);
static void PrintRGB(UWORD, UWORD, UWORD);
static void PrintXY(WORD, WORD, UWORD);
static void DoBorder(UWORD);
static void MakeBorder(void);

static void DoBox(void);
static void SetColor(UWORD);
static void HelpKey(UWORD);

APTR RequestColor(struct Screen *);

#define CopyPtrHeight 20
#define CopyPtrWidht 16
#define CopyPtrMem CopyPtrHeight*4+4

static struct Window *MyWindow;
static struct Screen *MyScreen;
static struct MsgPort *MyPort;
static struct IntuiMessage *MyMsg;
static struct RastPort *MyRapo;
static struct ViewPort *MyViewPort;

static UWORD *ctable;
static UWORD actcol=1;
static UWORD ColorBuf[32];
static UWORD UndoBuf[32];
static UWORD BorderX,BorderY;

static UBYTE xstep,ystep;
static BYTE endflag;
static BYTE plane,numcols;
static APTR PtrBase;

static char rgb[] = " ";


static SHORT BorderVectors1[] = {
	0,0,
	55,0,
	55,14,
	0,14,
	0,0
};
static struct Border Border1 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors1,
	NULL
};

static struct IntuiText IText1 = {
	1,0,JAM2,
	3,3,
	NULL,
	"SPREAD",
	NULL
};

static struct Gadget Gadget11 = {
	NULL,
	9,121,
	54,13,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border1,
	NULL,
	&IText1,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors2[] = {
	0,0,
	73,0,
	73,14,
	0,14,
	0,0
};
static struct Border Border2 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors2,
	NULL
};

static struct IntuiText IText2 = {
	1,0,JAM2,
	5,3,
	NULL,
	"EXCHANGE",
	NULL
};

static struct Gadget Gadget10 = {
	&Gadget11,
	9,105,
	72,13,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border2,
	NULL,
	&IText2,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors3[] = {
	0,0,
	43,0,
	43,14,
	0,14,
	0,0
};
static struct Border Border3 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors3,
	NULL
};

static struct IntuiText IText3 = {
	1,0,JAM2,
	5,3,
	NULL,
	"COPY",
	NULL
};

static struct Gadget Gadget9 = {
	&Gadget10,
	66,121,
	42,13,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border3,
	NULL,
	&IText3,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors4[] = {
	0,0,
	42,0,
	42,14,
	0,14,
	0,0
};
static struct Border Border4 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors4,
	NULL
};

static struct IntuiText IText4 = {
	1,0,JAM2,
	4,3,
	NULL,
	"UNDO",
	NULL
};

static struct Gadget Gadget8 = {
	&Gadget9,
	111,121,
	41,13,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border4,
	NULL,
	&IText4,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors5[] = {
	0,0,
	69,0,
	69,14,
	0,14,
	0,0
};
static struct Border Border5 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors5,
	NULL
};

static struct IntuiText IText5 = {
	1,0,JAM2,
	6,3,
	NULL,
	"DEFAULT",
	NULL
};

static struct Gadget Gadget7 = {
	&Gadget8,
	84,105,
	68,13,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border5,
	NULL,
	&IText5,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors6[] = {
	0,0,
	66,0,
	66,14,
	0,14,
	0,0
};
static struct Border Border6 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors6,
	NULL
};

static struct IntuiText IText6 = {
	1,0,JAM2,
	10,3,
	NULL,
	"CANCEL",
	NULL
};

static struct Gadget Gadget6 = {
	&Gadget7,
	87,137,
	65,13,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border6,
	NULL,
	&IText6,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors7[] = {
	0,0,
	47,0,
	47,14,
	0,14,
	0,0
};
static struct Border Border7 = {
	-1,-1,
	1,0,JAM1,
	5,
	BorderVectors7,
	NULL
};

static struct IntuiText IText7 = {
	1,0,JAM2,
	15,3,
	NULL,
	"OK",
	NULL
};

static struct Gadget Gadget5 = {
	&Gadget6,
	9,137,
	46,13,
	NULL,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border7,
	NULL,
	&IText7,
	NULL,
	NULL,
	NULL,
	NULL
};

static SHORT BorderVectors8[] = {
	0,0,
	64,0,
	64,64,
	0,64,
	0,1
};
static struct Border Border8 = {
	-1,-1,
	0,0,JAM1,
	5,
	BorderVectors8,
	NULL
};

static struct Gadget SelGadget = {
	&Gadget5,
	88,24,
	64,64,
	GADGHBOX+GADGHIMAGE,
	RELVERIFY,
	BOOLGADGET,
	(APTR)&Border8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

static struct PropInfo BlueGadgetSInfo = {
	AUTOKNOB+FREEVERT,
	(UWORD)-1,0,
	(UWORD)-1,4095,
};

static struct Image Image1 = {
	0,0,
	8,4,
	0,
	NULL,
	0x0000,0x0000,
	NULL
};

static struct Gadget BlueGadget = {
	&SelGadget,
	48,24,
	16,64,
	NULL,
	RELVERIFY+GADGIMMEDIATE,
	PROPGADGET,
	(APTR)&Image1,
	NULL,
	NULL,
	NULL,
	(APTR)&BlueGadgetSInfo,
	NULL,
	NULL
};

static struct PropInfo GreenGadgetSInfo = {
	AUTOKNOB+FREEVERT,
	(UWORD)-1,0,
	(UWORD)-1,4095,
};

static struct Image Image2 = {
	0,0,
	8,4,
	0,
	NULL,
	0x0000,0x0000,
	NULL
};

static struct Gadget GreenGadget = {
	&BlueGadget,
	30,24,
	16,64,
	NULL,
	RELVERIFY+GADGIMMEDIATE,
	PROPGADGET,
	(APTR)&Image2,
	NULL,
	NULL,
	NULL,
	(APTR)&GreenGadgetSInfo,
	NULL,
	NULL
};

static struct PropInfo RedGadgetSInfo = {
	AUTOKNOB+FREEVERT,
	(UWORD)-1,0,
	(UWORD)-1,4095,
};

static struct Image Image3 = {
	0,0,
	8,4,
	0,
	NULL,
	0x0000,0x0000,
	NULL
};

static struct Gadget RedGadget = {
	&GreenGadget,
	12,24,
	16,64,
	NULL,
	RELVERIFY+GADGIMMEDIATE,
	PROPGADGET,
	(APTR)&Image3,
	NULL,
	NULL,
	NULL,
	(APTR)&RedGadgetSInfo,
	NULL,
	NULL
};

#define GadgetList1 RedGadget

static struct IntuiText IText10 = {
	1,0,JAM2,
	52,14,
	NULL,
	"B",
	NULL
};

static struct IntuiText IText9 = {
	1,0,JAM2,
	34,14,
	NULL,
	"G",
	&IText10
};

static struct IntuiText IText8 = {
	1,0,JAM2,
	16,14,
	NULL,
	"R",
	&IText9
};

#define IntuiTextList1 IText8

static struct NewWindow NewWindowStructure1 = {
	75,30,
	161,160,
	0,1,
	REFRESHWINDOW+GADGETDOWN+GADGETUP+CLOSEWINDOW+RAWKEY,
	WINDOWDRAG+WINDOWCLOSE+ACTIVATE+NOCAREREFRESH,
	&RedGadget,
	NULL,
	"Edit Palette",
	NULL,
	NULL,
	5,5,
	(UWORD)-1,(UWORD)-1,
	CUSTOMSCREEN
};


static void HandleEvent(object)
APTR object;
{
  if (object == (APTR)&SelGadget) { SelectColor(object); return; }
  if (object == (APTR)&Gadget5) { EndColor(object); return; }
  if (object == (APTR)&Gadget6) { QuitColor(object); return; }
  if (object == (APTR)&Gadget7) { Default(object); return; }
  if (object == (APTR)&Gadget8) { Undo(object); return; }
  if (object == (APTR)&Gadget9) { CopyColor(object); return; }
  if (object == (APTR)&Gadget10) { Exchange(object); return; }
  if (object == (APTR)&Gadget11) { Spread(object); return; }
}

/* end of PowerWindows source generation */


static UWORD CopyPtr[] = {
	0x0000,0x0000,
	0x0000,0xFC00,
	0x7C00,0xFE00,
	0x7C00,0x8600,
	0x7800,0x8C00,
	0x7C00,0x8600,
	0x6E00,0x9300,
	0x0700,0x6980,
	0x0380,0x04C0,
	0x01C0,0x0260,
	0x0080,0x0140,
	0x0000,0x0080,
	0x0000,0x0000,
	0x3F1C,0xC060,
	0x0C36,0x30C8,
	0x0C63,0x318C,
	0x0C63,0x318C,
	0x0C63,0x318C,
	0x0C36,0x30C8,
	0x0C1C,0x3060,
	0x0000,0x0000,
};

static void HelpKey(UWORD Key)
{
	if (Key==223)
	{
		LoadRGB4(MyViewPort,ColorBuf,2);
		SetColor(actcol);
	}
}

static void SaveUndo(void)
{
	register UWORD i;

	for (i=0;i<numcols;i++)
	{
		UndoBuf[i]=ctable[i];
	}
}

static void PrintXY(x,y,z)
WORD x,y;
UWORD z;
{
	if (z>9) {z+=7;}
	rgb[0]=z+48;
	Move(MyRapo,x,y);
	Text(MyRapo,(char *)&rgb,1);
}

static void PrintRGB(r,g,b)
UWORD r,g,b;
{
	SetAPen(MyRapo,1);
	PrintXY((WORD)(RedGadget.LeftEdge+5),98,r);
	PrintXY((WORD)(GreenGadget.LeftEdge+5),98,g);
	PrintXY((WORD)(BlueGadget.LeftEdge+5),98,b);
}

static void HandleColor(APTR obj)
{
	register UWORD r,g,b;

	SaveUndo();
	for(;;)
	{
		r = 0xF-(RedGadgetSInfo.VertPot>>12);
		g = 0xF-(GreenGadgetSInfo.VertPot>>12);
		b = 0xF-(BlueGadgetSInfo.VertPot>>12);

		SetRGB4(MyViewPort,actcol,r,g,b);

		PrintRGB(r,g,b);

		WaitTOF();
		MyMsg = (struct IntuiMessage *)GetMsg(MyPort);
		if (MyMsg->Class==GADGETUP)
		{
			ReplyMsg((struct IntuiMessage *)MyMsg);
			break;
		}

		if (MyMsg)
		{
			ReplyMsg((struct IntuiMessage *)MyMsg);
		}
	}
}

static void SelectColor(APTR obj)
{
	actcol = ReadPixel(MyRapo,MyWindow->MouseX,MyWindow->MouseY);
	SetColor(actcol);
}

static void Undo(APTR nix)
{
	register UWORD i,temp;

	for (i=0;i<numcols;i++)
	{
		temp=UndoBuf[i];
		UndoBuf[i]=ctable[i];
		ctable[i]=temp;
	}
	LoadRGB4(MyViewPort,ctable,numcols);
	SetColor(actcol);
}

static void CopyColor(APTR nix)
{
	register UWORD oldcol;

	SaveUndo();
	SetPointer(MyWindow,(UWORD *)PtrBase,CopyPtrHeight,CopyPtrWidht,0,0);

	for (;;)
	{
		WaitPort(MyPort);
		MyMsg = (struct IntuiMessage *)GetMsg(MyPort);
		if (MyMsg->Class==GADGETUP)
		{
			if (MyMsg->IAddress==(APTR)&SelGadget)
			{
				oldcol=actcol;
				actcol=ReadPixel(MyRapo,MyWindow->MouseX,MyWindow->MouseY);
				ctable[actcol]=ctable[oldcol];
				LoadRGB4(MyViewPort,ctable,numcols);
				SetColor(actcol);
				break;
			}
			else
			{
				break;
			}
		}
		ReplyMsg((struct IntuiMessage *)MyMsg);
	}
	ClearPointer(MyWindow);
}

static void Exchange(APTR nix)
{
	register UWORD oldcol,old;

	SaveUndo();
	SetPointer(MyWindow,(UWORD *)PtrBase,CopyPtrHeight,CopyPtrWidht,0,0);

	for (;;)
	{
		WaitPort(MyPort);
		MyMsg = (struct IntuiMessage *)GetMsg(MyPort);
		if (MyMsg->Class==GADGETUP)
		{
			if (MyMsg->IAddress==(APTR)&SelGadget)
			{
				oldcol=actcol;
				actcol=ReadPixel(MyRapo,MyWindow->MouseX,MyWindow->MouseY);

				old=ctable[actcol];
				ctable[actcol]=ctable[oldcol];
				ctable[oldcol]=old;

				LoadRGB4(MyViewPort,ctable,numcols);
				SetColor(actcol);
				break;
			}
			else
			{
				break;
			}
		}
		ReplyMsg((struct IntuiMessage *)MyMsg);
	}
	ClearPointer(MyWindow);
}

static void Spread(APTR nix)
{
	register UWORD old,new,newcol,i,temp;
	register WORD oldr,oldg,oldb,newr,newg,newb,diff,diffr,diffg,diffb;

	SaveUndo();
	SetPointer(MyWindow,(UWORD *)PtrBase,CopyPtrHeight,CopyPtrWidht,0,0);

	old=ctable[actcol];

	for (;;)
	{
		WaitPort(MyPort);
		MyMsg = (struct IntuiMessage *)GetMsg(MyPort);
		if (MyMsg->Class==GADGETUP)
		{
			if (MyMsg->IAddress==(APTR)&SelGadget)
			{
				newcol=ReadPixel(MyRapo,MyWindow->MouseX,MyWindow->MouseY);
				new=ctable[newcol];

				LoadRGB4(MyViewPort,ctable,numcols);
				SetColor(newcol);
				break;
			}
			else
			{
				newcol=actcol;
				new=old;
				break;
			}
		}
		ReplyMsg((struct IntuiMessage *)MyMsg);
	}
	ClearPointer(MyWindow);

	if (actcol>newcol)
	{
		temp=newcol;
		newcol=actcol;
		actcol=temp;
	
		temp=new;
		new=old;
		old=temp;
	}

	oldr=(UWORD)((old>>8)&0xF)*(UWORD)0x100;
	oldg=(UWORD)((old>>4)&0xF)*(UWORD)0x100;
	oldb=(UWORD)(old&0xF)*(UWORD)0x100;

	newr=(UWORD)((new>>8)&0xF)*(UWORD)0x100;
	newg=(UWORD)((new>>4)&0xF)*(UWORD)0x100;
	newb=(UWORD)(new&0xF)*(UWORD)0x100;

	diff=(newcol-actcol);
	if (diff)
	{
		diffr=(WORD)(oldr-newr)/(WORD)diff;
		diffg=(WORD)(oldg-newg)/(WORD)diff;
		diffb=(WORD)(oldb-newb)/(WORD)diff;

		for (i=actcol+1;i<newcol;i++)
		{
			oldr=(oldr-diffr);
			oldg=(oldg-diffg);
			oldb=(oldb-diffb);

			if(oldr<0) {oldr=0;}
			if(oldg<0) {oldg=0;}
			if(oldb<0) {oldb=0;}

			ctable[i]=(oldr&0xF00)+((oldg/256)<<4)+(oldb/256);
		}
	}
	actcol=newcol;
	LoadRGB4(MyViewPort,ctable,numcols);
}

static void MakeBorder(void)
{
	Move(MyRapo,BorderX,BorderY);
	Draw(MyRapo,BorderX+xstep,BorderY);
	Draw(MyRapo,BorderX+xstep,BorderY+ystep);
	Draw(MyRapo,BorderX,BorderY+ystep);
	Draw(MyRapo,BorderX,BorderY);
}

static void DoBorder(UWORD color)
{
	register UWORD y;

	SetAPen(MyRapo,0);
	MakeBorder();

	y=(ULONG)64/(UWORD)ystep;
	BorderX=(color/y);
	BorderY=(color-(BorderX*y));	
	BorderX=(SelGadget.LeftEdge-1)+(UWORD)BorderX*(UWORD)xstep;
	BorderY=(SelGadget.TopEdge-1)+(UWORD)BorderY*(UWORD)ystep;

	SetAPen(MyRapo,1);
	MakeBorder();	
}

static void SetColor(UWORD col)
{
	register UWORD r,g,b;

	r=g=b=ctable[col];
	r=(r>>8)&0xF;
	g=(g>>4)&0xF;
	b=(b&15);

	PrintRGB(r,g,b);
	DoBorder(col);

	r=(UWORD)(0xF-r)*(UWORD)0x1111;
	g=(UWORD)(0xF-g)*(UWORD)0x1111;
	b=(UWORD)(0xF-b)*(UWORD)0x1111;

	RedGadgetSInfo.VertPot=r;
	GreenGadgetSInfo.VertPot=g;
	BlueGadgetSInfo.VertPot=b;
	RefreshGList(&RedGadget,MyWindow,0L,3L);
}

static void StoreColors(void)
{
	register UWORD i;

	for (i=0;i<numcols;i++)
	{
		ColorBuf[i]=ctable[i];
	}
}

static void ReStoreColors(void)
{
	LoadRGB4(MyViewPort,ColorBuf,numcols);
}

static void EndColor(APTR nix)
{
	CleanUp();
}

static void QuitColor(APTR nix)
{
	ReStoreColors();
	CleanUp();
}

static void Default(APTR obj)
{
	SaveUndo();
	ReStoreColors();
	SetColor(actcol);
}

static void CleanUp()
{
	NewWindowStructure1.LeftEdge = MyWindow->LeftEdge;
	NewWindowStructure1.TopEdge  = MyWindow->TopEdge;
	CloseWindow(MyWindow);
	endflag=TRUE;
}

static void DoBox()
{
	UWORD col=0;
	register UBYTE x,y,xpos,ypos;

	if (plane==1) {xstep=64; ystep=32;}
	if (plane==2) {xstep=32; ystep=32;}
	if (plane==3) {xstep=32; ystep=16;}
	if (plane==4) {xstep=16; ystep=16;}
	if (plane==5) {xstep=16; ystep=8;}

	xpos=SelGadget.LeftEdge;
	ypos=SelGadget.TopEdge;

	for (x=0;x<64;x+=xstep)
	{
		for (y=0;y<64;y+=ystep)
		{
			SetAPen(MyRapo,col);
			RectFill(MyRapo,x+xpos,y+ypos,x+xpos+xstep-2,y+ypos+ystep-2);		
			col=col+1;
		}
	}
}

APTR RequestColor(struct Screen *scr)
{
	if (scr)
	{
		register ULONG class; 
		register UWORD code;
		register APTR address;

		if (PtrBase=AllocMem(CopyPtrMem,2))
		{
			CopyMem((char *)&CopyPtr,(char *)PtrBase,CopyPtrMem);

			BorderX=SelGadget.LeftEdge-1;
			BorderY=SelGadget.TopEdge-1;
			endflag = 0;
			NewWindowStructure1.Screen = scr;

			NewWindowStructure1.Type = (scr->Flags)&0x000F;

			if (MyWindow = OpenWindow(&NewWindowStructure1))

			{
				MyPort     = MyWindow->UserPort;
				MyRapo     = MyWindow->RPort;
				MyViewPort = &(scr->ViewPort);
				ctable     = (UWORD *)MyViewPort->ColorMap->ColorTable;
				plane      = MyRapo->BitMap->Depth; if(plane>5) plane=5;
				numcols    = 1<<plane;
	
				DoBox();
				PrintIText(MyRapo,&IntuiTextList1,0L,0L);
	
				StoreColors();
				SaveUndo();
				SetColor(1);
	
				do
				{
					WaitPort(MyPort);
					MyMsg	= (struct IntuiMessage *)GetMsg(MyPort);
		
					class	= MyMsg->Class;
					code	= MyMsg->Code;
					address = MyMsg->IAddress;
	
					ReplyMsg((struct IntuiMessage *)MyMsg);

					switch(class)
					{
						case RAWKEY:
							HelpKey(code);
							break;
	
						case GADGETDOWN:
							HandleColor(address);
							break;
		
						case GADGETUP:
							HandleEvent(address);
							break;
	
						case CLOSEWINDOW:
							ReStoreColors();
							CleanUp();
	
						default:
							break;
					}
				}
				while (!endflag);
			}
			FreeMem(PtrBase,CopyPtrMem);
			return((APTR) MyViewPort->ColorMap->ColorTable);
		}
	}
}

/*
void __stdargs _main(char *argline)
{
	register APTR dummy;
	register WORD depth;

	IntuitionBase = OpenLibrary("intuition.library",0L);
	GfxBase       = OpenLibrary("graphics.library",0L);

	for (depth=1;depth<6;depth++)
	{
		NewScreenStructure.Depth=depth;
		MyScreen      = OpenScreen(&NewScreenStructure);
		dummy=RequestColor(MyScreen);
		CloseScreen(MyScreen);
	}

	CloseLibrary(IntuitionBase);
	CloseLibrary(GfxBase);
}
*/
