/**************************************************************************
**                                                                       **
**   ShowRequest  -  Super-Requester unter dem Mauspfeil ausgeben        **
**                                                                       **
*+   Original version by René Straub 18-Jul-89                           +*
*+                                                                       +*
*+   Modification History:                                               +*
*+   --------------------                                                +*
*+                                                                       +*
*+   23-Jul-89  CHW  Created this file (99% rewritten :-)                +*
*+   08-Aug-89  CHW  Zulange Textzeilen werden automatisch getrennt      +*
*+   06-Oct-89  CHW  ShowMonoReq() hier hereingenommen                   +*
*+   31-Oct-89  CHW  ToUpper-Bug bei VANILLAKEY fixed                    +*
*+   04-Nov-89  CHW  ToUpper-Bug bei VANILLAKEY really fixed, Amiga-v/b  +*
*+   05-Nov-89  CHW  No more gurus if word longer than 1 line            +*
*+   18-Jul-90  CHW  itext->Font is now set for IntuiTextLength()        +*
*+   02-Nov-93  CHW  Benutzt jetzt strlen statt StrLen (->Bobi)          +*
*+                                                                       +*
***************************************************************************
**                                                                       **
**   Parameter :  A0.L :  Zeiger auf Text, Linien durch \n getrennt      **
**                A1.L :  Zeiger auf PosText                             **
**                A2.L :  Zeiger auf NegText                             **
**                D0.W :  Bit  0: 1=Default PosText, 0=Default NegText   **
**                        Bit 15: 1=Text hat Schatten, 0=normal          **
**                                                                       **
**   Resultat :   D0.L :  0=NegText angewählt, 1=PosText angewählt       **
**                                                                       **
**************************************************************************/

/* #define DEBUG */

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <libraries/dosextens.h>

#include <string.h>

#include "Bobi.h"


extern struct IntuitionBase *IntuitionBase;

#define reg register
#define MAXTEXTLENGTH 160		/* Maximale Länge einer Textzeile */
#define REQF_DEFAULT  1			/* Gadget unter dem Mauszeiger */
#define REQF_SHADOW 0x8000		/* Text hat Schatten */

struct MyIText
{
	struct IntuiText ShadowIText;
	struct IntuiText IText;
	char Text[MAXTEXTLENGTH];
};

/*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*/

static BYTE alerttext[] = {
	0, 24, 14,
	'N','o',' ','m','e','m',' ','f','o','r',' ','r','e','q','u','e','s','t','e','r',
	0, 0, 0
};

/*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*/

static WORD PosBorderVecs[] = {	0,0, -1,0, -1,12, 0,12, 0,1 };
static WORD NegBorderVecs[] = {	0,0, -1,0, -1,12, 0,12, 0,1 };

static struct Border PosBorder = {
	-3,-2,1,0,0,5,PosBorderVecs,NULL
};
static struct Border NegBorder = {
	-3,-2,1,0,0,5,NegBorderVecs,NULL
};

static struct IntuiText PosIText = {
	1,0,JAM1,2,1,NULL,NULL,NULL
};
static struct IntuiText NegIText = {
	1,0,JAM1,2,1,NULL,NULL,NULL
};

static struct Gadget PosGadget = {
	NULL,10,-1234,1234,1234,GRELBOTTOM,RELVERIFY,BOOLGADGET,
	(APTR)&PosBorder,NULL,&PosIText,NULL,NULL,NULL,NULL
};
static struct Gadget NegGadget = {
	&PosGadget,-1234,-1234,1234,1234,GRELBOTTOM|GRELRIGHT,RELVERIFY,BOOLGADGET,
	(APTR)&NegBorder,NULL,&NegIText,NULL,NULL,NULL,NULL
};

/*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*/

LONG ShowRequest(char *text, char *postext, char *negtext, ULONG flags)
{
	struct NewWindow nw;
	struct Window *w,*prwindow;
	struct Screen *s;
	struct MyIText itext;
	register struct MyIText *iptr=&itext;
	UWORD poswidth,negwidth;
	register LONG endflag=-1;

	/* NewWindow-Struktur initialisieren */

	ClearMem(&nw,sizeof(nw));
	nw.Width=180;
	nw.Height=4;

	/* Zeiger auf Screen holen und nw entsprechend anpassen */

	if (prwindow=(struct Window *)(((struct Process *)FindTask(0L))->pr_WindowPtr))
	{
		nw.DetailPen = prwindow->DetailPen;
		nw.BlockPen  = prwindow->BlockPen;
		nw.Screen= s = prwindow->WScreen;
		nw.Type      = CUSTOMSCREEN;
		ScreenToFront(s);
	}
	else
	{
		WBenchToFront();
		nw.DetailPen = 0;
		nw.BlockPen  = 1;
		nw.Screen    = 0;
		nw.Type      = WBENCHSCREEN;
		s = IntuitionBase->FirstScreen;
	}

	/* Gadgets und IntuiTexts initialisieren */

	itext.IText.NextText = 0;
	NegIText.ITextFont   = s->Font;
	PosIText.ITextFont   = s->Font;
	NegIText.IText       = negtext;
	PosIText.IText       = postext;
	NegGadget.Width      = negwidth=IntuiTextLength(&NegIText)+4;
	PosGadget.Width      = poswidth=IntuiTextLength(&PosIText)+4;
	NegBorderVecs[2]     = NegBorderVecs[4] = negwidth+5;
	PosBorderVecs[2]     = PosBorderVecs[4] = poswidth+5;
	NegGadget.Height     = s->RastPort.TxHeight+2;
	PosGadget.Height     = s->RastPort.TxHeight+2;
	NegBorderVecs[5]     = NegBorderVecs[7]  = NegGadget.Height+3;
	PosBorderVecs[5]     = PosBorderVecs[7]  = PosGadget.Height+3;
	PosBorder.FrontPen   = PosIText.FrontPen = flags&REQF_DEFAULT ? 3:1;
	NegBorder.FrontPen   = NegIText.FrontPen = flags&REQF_DEFAULT ? 1:3;
	NegGadget.LeftEdge   = -negwidth-10;
	NegGadget.TopEdge    = PosGadget.TopEdge = -NegGadget.Height-4;
	NegGadget.NextGadget = postext?&PosGadget:0;	/* PosGadget ein/aus */

	if(nw.Width < (negwidth+poswidth+18)) nw.Width=negwidth+poswidth+18;

	do
	{
		register char *dptr;
		register LONG ilen;

		nw.Height += s->RastPort.TxHeight+2;
		iptr->IText.NextText=AllocMem(sizeof(*iptr),MEMF_CLEAR);
		if(!(iptr=(struct MyIText *)iptr->IText.NextText)) break;
		iptr->ShadowIText.NextText = &(iptr->IText);
		iptr->IText.LeftEdge       = 8;
		iptr->ShadowIText.LeftEdge = 10;
		iptr->IText.TopEdge        = nw.Height;
		iptr->ShadowIText.TopEdge  = nw.Height+1;
		iptr->IText.FrontPen       = 1;
		iptr->ShadowIText.FrontPen = 2;
		iptr->IText.IText=iptr->ShadowIText.IText=dptr=iptr->Text;

		for(;;)
		{
			register ULONG olddptr=(ULONG)dptr,oldtext=(ULONG)text;

			while((*dptr=*text) > ' ') { text++; dptr++; } *dptr=0;
			if((ilen=TextLength(&s->RastPort,iptr->Text, strlen(iptr->Text)))
				> (s->Width-20))
			{
				dptr=(char *)olddptr;
				if(dptr!=iptr->Text) /* Falls nicht 1. Wort d.Zeile (überlang) */
				{
					*dptr=0; /* String vor nächstem Wort begrenzen */
					text=(char *)oldtext-1; /* Source-Ptr zurückstellen */
				}
				break;
			}
			if(*text != ' ') break;
			*dptr++=*text++;
		}

		if(ilen > nw.Width) nw.Width=ilen;
		if(!(flags & REQF_SHADOW)) iptr->ShadowIText.IText=0;
	}
	while(*text++);
	nw.Height += (s->RastPort.TxHeight<<1)+14;

	nw.Width       += 16;
	nw.LeftEdge    = flags&REQF_DEFAULT?s->MouseX-24:s->MouseX-nw.Width+24;
	nw.TopEdge     = s->MouseY-nw.Height+12;
	nw.IDCMPFlags  = GADGETUP|VANILLAKEY;
	nw.Flags       = WINDOWDRAG+WINDOWDEPTH+ACTIVATE+NOCAREREFRESH+RMBTRAP;
	nw.FirstGadget = &NegGadget;
	nw.Title       = "Click to continue";
	nw.Screen      = s;

	if(nw.Width  > s->Width)  nw.Width =s->Width;
	if(nw.Height > s->Height) nw.Height=s->Height;


	/* Randbegrenzungen abfragen */
	{
		register WORD maxx0=s->Width-nw.Width,maxy0=s->Height-nw.Height;

		if(nw.LeftEdge<0)     nw.LeftEdge = 0;
		if(nw.TopEdge<0)      nw.TopEdge  = 0;
		if(nw.LeftEdge>maxx0) nw.LeftEdge = maxx0;
		if(nw.TopEdge>maxy0)  nw.TopEdge  = maxy0;
	}

	if(w=OpenWindow(&nw))
	{
		register struct IntuiMessage *msg;
		register LONG code;

		/* Pos/NegText raufsetzen, damit Taste geht wenn's Spaces davor hat */

		if(postext) while(*postext <= ' ') postext++;
		while(*negtext <= ' ') negtext++;

		PrintIText(w->RPort,itext.IText.NextText,0,0);
		do
		{
			WaitPort(w->UserPort);
			msg  = (struct IntuiMessage *)GetMsg(w->UserPort);
			code = msg->Code;

			switch(msg->Class)
			{
				case VANILLAKEY:
					code &= 0xDF;	/* ToUpper */
					if(msg->Qualifier & AMIGALEFT)
					{
						if(code=='V') endflag=TRUE;
						if(code=='B') endflag=FALSE;
						break;
					}
					if(!postext) { endflag=FALSE; break; }
					if(code==13) { endflag=flags&REQF_DEFAULT; }
					if(code==(*postext&0xDF)) endflag=TRUE;
					if(code==(*negtext&0xDF)) endflag=FALSE;
					break;

				default: /* case GADGETUP: */
					endflag = (msg->IAddress==(APTR)&PosGadget);
					break;
			}
			ReplyMsg((struct Message *)msg);
		}	
		while(endflag<0);
		CloseWindow(w);
	}
	else
	{
#ifdef DEBUG
		for(iptr=(struct MyIText *)itext.IText.NextText; iptr; iptr=(struct MyIText *)iptr->IText.NextText)
		{
			printf("'%s'\n",iptr->IText.IText);
		}
		printf("\nWindow: %ld/%ld %ld/%ld\n",nw.LeftEdge,nw.TopEdge,nw.Width,nw.Height);
#endif
		DisplayAlert(0,alerttext,24);
		endflag=FALSE;
	}

	for(iptr=(struct MyIText *)itext.IText.NextText; iptr;)
	{
		register struct MyIText *temp=iptr;
		iptr = (struct MyIText *)iptr->IText.NextText;
		FreeMem(temp,sizeof(*temp));
	}

	return endflag;
}

/*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*/

void __regargs ShowMonoReq(char *text)
{
	ShowRequest(text,0," OK ",REQF_SHADOW);
}
